"""
Reads Camino trajectory files
"""
import struct
import numpy as np


def read(filename):
    """
    Reads a Camino trajectory file

    :param filename: name of the file
    :return: (n_spins, n_steps + 1) memory map to an index array with as axes:

        - "time": double
        - "index": int
        - "pos": 3 x double
    """
    with open(filename, 'rb') as f:
        _, n_spins, tmax = [int(val) for val in struct.unpack('>ddd', f.read(24))]
    dtype = np.dtype([('time', 'f8'), ('index', 'i4'), ('pos', ('f8', 3))]).newbyteorder('B')
    return np.memmap(filename, dtype=dtype, shape=(tmax + 1, n_spins), offset=24).T
