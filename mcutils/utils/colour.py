from matplotlib import cm
import numpy as np


def qualitative():
    """
    Returns a set of `n` distinct colours

    yields (R, G, B, A) tuples
    """
    for cm_name in ('Set1', 'Set3', 'tab20', 'Paired', 'Accent', 'Pastel1'):
        cmap = getattr(cm, cm_name)
        for rgb in cmap.colors:
            yield tuple(rgb) + (1., )
    while True:
        rgb = np.random.rand(3)
        rgb /= np.sqrt(np.sum(rgb ** 2))
        yield tuple(rgb) + (1., )


