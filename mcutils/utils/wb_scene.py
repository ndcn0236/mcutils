"""
Support for workbench Scene files
"""
from xml.etree import ElementTree
from typing import Iterable


class Scene(object):
    """
    Workbench scene file
    """
    def __init__(self, etree: ElementTree):
        if isinstance(etree, str):
            raise ValueError("Use `Scene.read` to read a Scene from a string")
        self.etree = etree

    @property
    def root(self, ):
        return self.etree.getroot()

    @classmethod
    def read(cls, filename):
        return cls(ElementTree.parse(filename))

    def write(self, filename):
        return self.etree.write(filename, encoding='UTF-8', xml_declaration=True)

    def iter(self, ) -> Iterable[ElementTree.Element]:
        yield from self.root.iter()

    def iter_filenames(self, ):
        for element in self.iter():
            if (
                element.attrib.get('Type', '') == 'pathName' and
                element.text is not None and
                len(element.text.strip()) != 0
            ):
                element.text = (yield element.text, False)
            if element.attrib.get('Class', '') == 'CaretDataFile':
                element.attrib['Name'] = (yield element.attrib['Name'], True)

