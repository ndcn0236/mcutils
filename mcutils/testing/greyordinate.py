import numpy as np
from mcutils.utils import greyordinate


def surface_greyordinate(arr, surface_mask=None):
    """
    Creates a surface greyordinate file

    :param arr: (..., Nvertices) array
    :param surface_mask: bool mask of the surface mesh (default Nvertices True array)
    """
    if surface_mask is None:
        surface_mask = np.ones(arr.shape[-1], dtype='bool')
    else:
        surface_mask = np.asarray(surface_mask) != 0
    assert arr.shape[-1] == surface_mask.sum()
    return greyordinate.GreyOrdinates(
        arr, greyordinate.BrainModelAxis.from_mask(surface_mask, name='cortex_left')
    )
