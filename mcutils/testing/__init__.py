"""
Code to read or mock testing data
"""
from pathlib import Path

data_directory = Path(__file__).parent / 'data'
fsaverage_directory = data_directory / 'fsaverage'
