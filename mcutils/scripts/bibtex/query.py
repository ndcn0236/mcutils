#!/usr/bin/env python
"""Query pubmed and adds result to bibtex file"""
from loguru import logger
import os
from mcutils.utils.bibtex import pubmed, file, arxiv


def run(query, bibtex_file=None, max_entries=20, doi=False, use_arxiv=False):
    """
    Queries pubmed and adds the result to the given bibtex file (stdout by default)
    """
    if doi:
        if isinstance(query, str):
            query = query.split()
        new_bib = file.BibTexSet([list(pubmed.doi_to_bibtex(doi))[0] for doi in query])
    elif use_arxiv:
        new_bib = file.BibTexSet(arxiv.query_mult(query))
    else:
        if not isinstance(query, str):
            query = ' '.join(query)
        pmids = pubmed.get_fetcher().pmids_for_query(query, retmax=max_entries)
        articles = pubmed.query_mult(pmids)
        new_bib = file.BibTexSet(
                [pubmed.to_bibtex(article) for article in articles]
        )

    if bibtex_file is None:
        print(repr(new_bib))
        return

    file_exists = os.path.isfile(bibtex_file)
    if file_exists:
        already_available = []
        old_bib = file.BibTexFile(bibtex_file)
        for entry in new_bib:
            if entry in old_bib:
                print('%s is already in the bibtex file' % entry)
                already_available.append(entry)
            else:
                old_bib.add(entry, adjust_key=True)
        for entry in already_available:
            new_bib.remove(entry)
    if len(new_bib) == 0:
        logger.info('No new entries found')
    else:
        print('Adding the following entries to %s:\n' % bibtex_file)
        print(repr(new_bib))
        option = ''
        if len(new_bib) == 1:
            option = 'y'
        while option != 'y' and option !='n':
            option = input('Please confirm (y/n): ')
            if option == 'y' or option == 'n':
                break
            else:
                print('please enter "y" or "n"')
        if option == 'y':
            old_bib = file.BibTexFile(bibtex_file)
            for entry in new_bib:
                old_bib.add_to_file(entry, adjust_key=True)


def run_from_args(args):
    """
    Runs the script based on a Namespace containing the command line arguments
    """
    if args.doi and args.arxiv:
        raise ValueError("set either -d/--doi or -x/--arxiv")
    run(
        args.query,
        args.bibtex_file,
        args.max_entries,
        doi=args.doi,
        use_arxiv=args.arxiv,
    )


def add_to_parser(parser):
    """
    Creates the parser of the command line arguments
    """
    parser.add_argument('query', nargs='+', help='any query')
    parser.add_argument('-n', '--max-entries', type=int, default=20, help='maximum number of entries to download')
    parser.add_argument('-f', '--bibtex-file',
                        help='which bibtex file to append the pubmed results to stdout')
    parser.add_argument('-d', '--doi', action='store_true', help='Extract doi from https://doi.org rather than pubmed')
    parser.add_argument('-x', '--arxiv', action='store_true', help='Extract arXiv identifiers from physics arXiv rather than pubmed')

