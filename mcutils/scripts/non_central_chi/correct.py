#!/usr/bin/env python
"""
Corrects MRI signal for the noise floor
"""
from loguru import logger
import nibabel as nib
from scipy.stats import gamma, lognorm
from mcutils.utils.noise import log_non_central_chi
import numpy as np
from scipy import optimize


def logp(observed_signal, log_noise, ncoils, f_no_signal, dist_param1, dist_param2, use_dist='gamma'):
    """
    Computes the logp of the total histogram

    :param observed_signal: observed signal level
    :param log_noise: log of the noise variance
    :param ncoils: number of coils
    :param f_no_signal: fraction of observed signal with no true underlying signal
    :param dist_param1: shape of the gamma distribution or log(mean) of lognorm distribution
    :param dist_param2: rate of the gamma distribution or std of lognorm distribution
    :param use_dist: which distribution to use to model the true signal
    :return: log(P) for the observed signal
    """
    if f_no_signal < 0 or f_no_signal > 1:
        return observed_signal - np.inf
    no_signal = log_non_central_chi(observed_signal, 1e-20, np.exp(log_noise), ncoils)
    if use_dist == 'none':
        return no_signal

    # under assumption 2 ncoils >> correction_factor
    true_signal = np.sqrt(observed_signal ** 2 - 2 * ncoils * np.exp(log_noise))

    # under assumption that gamma/log-norm distribution is so wide that convolution with
    # non-central chi distribution does not alter it
    if use_dist == 'gamma':
        with_signal = gamma.logpdf(true_signal, a=dist_param1, scale=1/dist_param2)
    elif use_dist == 'lognorm':
        with_signal = lognorm.logpdf(true_signal, scale=dist_param1, s=dist_param2)
    else:
        raise ValueError(f"Unknown distribution {use_dist}")

    with_signal[~np.isfinite(with_signal)] = -np.inf

    res = np.log(f_no_signal * np.exp(no_signal) + (1 - f_no_signal) * np.exp(with_signal))
    return res


def guess_parameters(observed_signal, ncoils=None, use_dist='gamma'):
    """
    Makes an initial guess of the noise level and number of coils

    :param observed_signal: observed signal level
    :param ncoils: number of coils used in the reconstruction (default: fit to data)
    :param use_dist: which distribution to use to model the true signal
    :return: Initial parameters of the histogram fit:

        - log_noise: set to best-fit value evaluated on grid
        - ncoils: set to best-fit value evaluated on grid
        - f_no_signal: set to 0.5
        - gamma_shape or log(mean)
        - gamma_rate or variance
    """
    if use_dist == 'gamma':
        mean_signal = observed_signal.mean()
        var_signal = observed_signal.var()
        dist_param2 = mean_signal / var_signal
        dist_param1 = mean_signal * dist_param2
    elif use_dist == 'lognorm':
        dist_param1 = np.log(observed_signal).mean()
        dist_param2 = np.log(observed_signal).max() - np.log(observed_signal).min()
    elif use_dist == 'none':
        dist_param1, dist_param2 = 0., 0.
    else:
        raise ValueError(f"Unknown distribution {use_dist}")

    try_noise = np.log(np.logspace(0, 2, 10))
    if ncoils is None:
        try_ncoils = np.array([1, 4, 16, 64])
    else:
        try_ncoils = np.array([ncoils])
    grid_values = logp(observed_signal[:, None, None], try_noise[None, :, None], try_ncoils[None, None, :],
                       0.5, dist_param1, dist_param2, use_dist=use_dist).sum(0)
    logger.debug(f'log(P) on grid {str(grid_values)}')
    idx_noise, idx_ncoils = np.unravel_index(np.argmax(grid_values), (try_noise.size, try_ncoils.size))
    params = try_noise[idx_noise], try_ncoils[idx_ncoils], 0.5, dist_param1, dist_param2
    logger.info(f'Guessed parameters: {str(params)}')
    return params


def fit_parameters(observed_signal, guess, to_fix=(False,) * 5, use_dist='gamma'):
    """
    Fits the current set of parameters

    :param observed_signal: (N, ) array with observed signal
    :param guess: (5, ) array with current set of parameters
    :param to_fix: which parameters to fix, in order: log_noise, ncoils, f_no_signal, gamma_shape, gamma_rate
    :param use_dist: which distribution to use to model the true signal
    :return: new set of parameters (fixed parameters have not been altered)
    """
    bounds = np.array([(-np.inf, np.inf)] * 5)
    bounds[2] = (0, 1)
    to_alter = ~np.array(to_fix, dtype='bool')
    logger.info(f'Fitting histogram with {observed_signal.size} elements and {5 - to_alter.sum()} parameters fixed: {str(to_fix)}')
    full_params = np.array(guess).copy()

    def to_minimize(params):
        full_params[to_alter] = params
        return -logp(observed_signal, *full_params, use_dist=use_dist).sum()

    res = optimize.minimize(to_minimize, full_params[to_alter], method='nelder-mead')
    logger.debug(str(res))
    full_params[to_alter] = res.x
    logger.info(f'Best-fit parameters: {str(full_params)}')
    return full_params


def get_params(input, histogram=None, use_dist='gamma', precise=False, ncoils=None):
    """
    Fit the signal intensity histogram

    :param input: signal intensity values
    :param histogram: filename to plot the best fit to the histogram (default: no plotting)
    :param use_dist: which distribution to use for the true signal
    :param precise: whether to fit to 10^6 rather than 10^5 signal intensity values
    :param ncoils: number of coils in the reconstruction (default: fit to the data)
    :return: best-fit parameters to the histogram:

        - log_noise: set to best-fit value evaluated on grid
        - ncoils: set to best-fit value evaluated on grid
        - f_no_signal: set to 0.5
        - gamma_shape or log(mean)
        - gamma_rate or variance
    """
    arr = input[input > 0].astype('f8')
    np.random.shuffle(arr)
    params = guess_parameters(arr[:100_000], use_dist=use_dist, ncoils=ncoils)
    first_opt = fit_parameters(arr[:100_000], params, (False, ncoils is not None, True, False, False),
                               use_dist=use_dist)
    second_opt = fit_parameters(arr[:100_000], first_opt, (False, ncoils is not None, False, False, False),
                                use_dist=use_dist)
    if precise:
        final_opt = fit_parameters(arr[:1_000_000], second_opt, (False, ncoils is not None, False, False, False),
                                   use_dist=use_dist)
    else:
        final_opt = second_opt
    logger.info('Best fit parameters:')
    for name, value in zip((
        'log(noise variance)',
        'number of coils',
        'fraction of image with no true signal',
        'shape of gamma distribution',
        'rate of gamma distribution',
    ), final_opt):
        logger.info(f'{name}: {value}')
    if histogram is not None:
        import matplotlib.pyplot as plt
        range = (0, np.amax(arr))
        plt.hist(arr, bins=np.arange(-0.5, np.amax(arr), 1), density=True,)
        xval = np.linspace(range[0], range[1], 3001)
        plt.plot(xval, np.exp(logp(xval, *final_opt, use_dist=use_dist)),
                 label='full distribution')
        plt.plot(xval, (1 - final_opt[2]) * np.exp(logp(xval, final_opt[0], final_opt[1], 0,
                                                         final_opt[3], final_opt[4], use_dist=use_dist)),
                 label=('Gamma' if use_dist == 'gamma' else 'log(Norm)') + ' distribution')
        plt.plot(xval, final_opt[2] * np.exp(logp(xval, final_opt[0], final_opt[1], 1,
                                                   final_opt[3], final_opt[4], use_dist=use_dist)),
                 label='Non-central chi distribution')
        plt.legend(loc='upper right')
        plt.xlabel('signal intensity')
        plt.ylabel('frequency')
        plt.title(', '.join(['{:.3f}'] * len(final_opt)).format(*tuple(final_opt)))
        plt.savefig(histogram)
    return final_opt


def correct_data(input, noise_variance, ncoils=1, flip=False):
    """
    Corrects the data for the noise floor described by the parameters

    :param input: input array
    :param noise_variance: variance of the true noise
    :param ncoils: number of coils
    :return: corrected array
    """
    true_signal_sq = input.astype('f8') ** 2 - 2 * ncoils * noise_variance
    return np.sqrt(abs(true_signal_sq)) * (np.sign(true_signal_sq) if flip else 1)


def run_from_args(args):
    """
    Runs the script based on a Namespace containing the command line arguments
    """
    img = nib.load(args.input)
    if args.mask is None:
        mask = ()
    else:
        mask = nib.load(args.mask).get_data() != 0
    final_opt = get_params(
            img.get_data()[mask],
            args.histogram,
            use_dist=args.dist,
            precise=args.precise,
            ncoils=args.ncoils
    )
    corrected = correct_data(
            img.get_data(),
            np.exp(final_opt[0]),
            final_opt[1],
            args.flip,
    )
    nib.Nifti1Image(corrected, affine=None, header=img.header).to_filename(args.corrected)
    print('noise parameters:', ' '.join([str(val) for val in final_opt]))


def add_to_parser(parser):
    """
    Creates the parser of the command line arguments
    """
    parser.add_argument("input", help='raw dataset')
    parser.add_argument("corrected", help='corrected dataset with roughly Gaussian noise')
    parser.add_argument("-m", "--mask", help='OPTIONAL (and typically not needed): mask marking the part of the image where the noise floor should be estimated')
    parser.add_argument("-hist", "--histogram", help='optional filename to plot the fit of the signal histogram')
    parser.add_argument("-d", "--dist", choices=('none', 'gamma', 'lognorm'), default='gamma',
                        help='Which distribution to use to model the true signal (default: gamma).\n' +
                        'Set to none if the masked part of the image contains no true signal (only for perfect masks)')
    parser.add_argument("-p", '--precise', action='store_true',
                        help='Fits the non-central chi distribution to 10^6 instead of 10^5 random signal intensities')
    parser.add_argument("-nc", '--ncoils', type=int,
                        help='Fixes the number of coils if known (otherwise fitted to the data)')
    parser.add_argument("-f", "--flip", action='store_true',
                        help='Flips voxels with signal less than the noise floor, so that there resulting signal'
                             'is negative (note that the output will no longer be Rician).')

