#!/usr/bin/env python
"""
Estimates the variance of the noise assuming a non-central chi distribution
"""
import nibabel as nib
from scipy import misc, special
import numpy as np


def beta_ncoils(ncoils):
    """
    Estimates beta_n
    
    Defined in section 2.4 in Koay & Basser (2006, JMR, doi:10.1016/j.jmr.2006.01.016)
    """
    return np.sqrt(np.pi / 2) * misc.factorial2(2 * ncoils - 1) / (2 ** (ncoils - 1) * misc.factorial(ncoils - 1))


def correction_factor(snr, ncoils):
    """
    Computes the correction factor

    Defined as equation 18 in Koay & Basser (2006, JMR, doi:10.1016/j.jmr.2006.01.016)

    :param snr: true signal over true noise (or the best current estimate of this)
    :param ncoils: number of coils
    :return: correction factor between true and observed variance
    """
    return 2 * ncoils + snr ** 2 - (beta_ncoils(ncoils) * special.hyp1f1(-0.5, ncoils, - snr ** 2 / 2)) ** 2


def new_snr_fixed_point(current_snr, ncoils, observed_snr):
    """
    Computes the next step in the fixed point root finding method

    Defined as equation 19 in Koay & Basser (2006, JMR, doi:10.1016/j.jmr.2006.01.016)

    :param current_snr: current best estimate of true signal over true noise
    :param ncoils: number of coils
    :param observed_snr: observed signal over noise ratio
    :return: new estimate of true signal over true noise ratio
    """
    return np.sqrt(correction_factor(current_snr, ncoils) * (1 + observed_snr ** 2) - 2 * ncoils)


def new_snr_newton(current_snr, ncoils, observed_snr):
    """
    Computes the next step in the Newton's root finding method

    Defined as equation A.3 in Koay & Basser (2006, JMR, doi:10.1016/j.jmr.2006.01.016)

    :param current_snr: current best estimate of true signal over true noise
    :param ncoils: number of coils
    :param observed_snr: observed signal over noise ratio
    :return: new estimate of true signal over true noise ratio
    """
    fixed_point = new_snr_fixed_point(current_snr, ncoils, observed_snr)
    numerator = fixed_point * (fixed_point - current_snr)
    denominator = current_snr * (1 + observed_snr ** 2) * (1 -
            beta_ncoils(ncoils) ** 2 / (2 * ncoils) *
            special.hyp1f1(-0.5, ncoils, -current_snr ** 2 / 2) *
            special.hyp1f1(+0.5, ncoils + 1, -current_snr ** 2 / 2)
    ) - fixed_point
    return current_snr - numerator / denominator


def true_snr(data, ncoils=1, epsilon=1e-8):
    """
    Estimates the SNR of the non-central chi distribution

    Uses the method described in section 2.4 and A1 in Koay & Basser (2006, JMR, doi:10.1016/j.jmr.2006.01.016)

    :param data: (..., N) dataset with N identical volumes (except for noise)
    :param ncoils: number of coils used in the reconstruction
    :param epsilon: target precision in the variance estimate
    :return: (..., ) array with the snr
    """
    observed_snr = abs(data).mean(axis=-1) / data.std(axis=-1)
    current_guess = observed_snr - np.sqrt(2 * ncoils / correction_factor(0, ncoils) - 1)
    updating = np.ones(current_guess.shape, dtype='bool')
    next_guess = current_guess.copy()
    while updating.any():
        next_guess[updating] = new_snr_newton(current_guess[updating], ncoils, observed_snr[updating])
        updating = abs(next_guess - current_guess) > epsilon
        current_guess = next_guess
    return current_guess


def run(data, ncoils=1, epsilon=1e-8):
    """
    Estimates the true noise level of the non-central chi distribution

    :param data: (..., N) dataset with N identical volumes (except for noise)
    :param ncoils: number of coils used in the reconstruction
    :param epsilon: target precision in the variance estimate
    :return: (..., ) array with the true signal & standard deviation
    """
    snr = true_snr(data, ncoils, epsilon)
    snr[~np.isfinite(snr)] = 0
    true_noise = np.sqrt(data.var(-1) / correction_factor(snr, ncoils))
    return true_noise * snr, true_noise


def run_from_args(args):
    """
    Runs the script based on a Namespace containing the command line arguments
    """
    img = nib.load(args.input)
    arr = run(
            img.get_data(),
            args.ncoils,
            args.epsilon
    )
    nib.Nifti1Image(np.stack(arr, axis=-1), affine=None, header=img.header).to_filename(args.output)


def add_to_parser(parser):
    """
    Creates the parser of the command line arguments
    """
    parser.add_argument('input', help='multiple volumes with the same true underlying image')
    parser.add_argument('output', help='new NIFTI file with true signal and variance')
    parser.add_argument('-nc', '--ncoils', default=1, type=int, help='number of coils in the reconstruction (default: 1)')
    parser.add_argument('-e', '--epsilon', default=1e-8, type=float, help='target precision in the variance estimate (default: 1e-8)')
