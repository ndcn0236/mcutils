"""
Builds a pipeline to run locally or submit to the cluster

The recommended way is the use :class:`Pipeline`,
although an older version using `generates` is still available
(and might be more straightforward in simple cases).

Both approaches rely on [FileTree](https://users.fmrib.ox.ac.uk/~paulmc/fsleyes/fslpy/latest/fsl.utils.filetree.html)
found in fslpy
"""
from typing import Optional, Union, Collection, Dict, Sequence, Set, Any
from pathlib import Path
from tempfile import mkstemp
from fsl.utils.fslsub import func_to_cmd, hold, _flatten_job_ids
from collections import defaultdict
from loguru import logger
import os
from functools import lru_cache
from mcutils.utils import log
from fsl.utils.filetree import FileTree
from dataclasses import dataclass, asdict
from graphviz import Digraph
from subprocess import run, PIPE
import argparse
import warnings
import inspect
import itertools
import numpy as np
from copy import copy

_label_counter = 0


class DependencyError(ValueError):
    """
    Raise when dependencies are missing in pipeline
    """


SUBMIT = os.getenv('SGE_ROOT', default='') != ''


def generates(names: Collection[str], submit_params=None, glob_vars='all', **submit_kwargs):
    """
    Wraps a function to be submitted to the cluster

    Will only run if the output files do no exist yet

    :param names: generated short names
    :param submit_params: submission parameters (default: use submit_kwargs instead)
    :param submit_kwargs: sets submission parameters (ignored if submit_params is set)
    :param glob_vars: sequence of which variables to glob when
    :return: wrapped function
    """
    names = list(names)
    if submit_params is None:
        submit_params = SubmitParams(**submit_kwargs)
    elif isinstance(submit_params, dict):
        submit_params = SubmitParams(**submit_params)
    if submit_params.wait_for is not None:
        raise ValueError("Did not expect job ids to wait for to be set during function definition")

    def get_wrapper(func):
        prev_runs = {}

        def wrapper(filetree: FileTree, run=False, **submit_kwargs):
            """Runs or submits the underlying function

            :param filetree: tree of input/output names
            :param run: just run the wrapped function and return the result (ignores all checks)
            :param submit_kwargs: sets submission parameters (ignored if submit_params is set)
            :return: job id in a tuple
            """
            if run:
                if not log.LOGSET and log.SUBMITTED:
                    log.setup_log()
                logger.debug("Running %s", func.__name__)
                return func(filetree)

            if (
                    len(names) > 0 and
                    all(len(filetree.get_all(name, glob_vars=glob_vars)) != 0 for name in names)
            ):
                logger.debug(func.__name__ + ': all output files already exist')
                return submit_kwargs.get('wait_for', ())

            out_filenames = []
            relevant_vars = {}
            for name in names:
                text, vars = filetree.get_template(name)
                out_filenames.append(text)
                relevant_vars.update(**vars)
            id_prev_run = (tuple(out_filenames), tuple(relevant_vars.items()))
            if len(names) > 0 and id_prev_run in prev_runs:
                logger.debug(func.__name__ + ': already got run in this session')
                return prev_runs[id_prev_run]

            if SUBMIT:
                logger.info('submitting new {} to cluster after {}'.format(func.__name__, submit_kwargs.get('wait_for', ())))
                logdir = submit_kwargs['logdir'] if 'logdir' in submit_kwargs else submit_params.logdir
                logdir_path = None if logdir is None else filetree.get(logdir, make_dir=True)
                if logdir_path is not None and not os.path.isdir(logdir_path):
                    os.mkdir(logdir_path)
                submit_kwargs['logdir'] = logdir_path
                cmd = func_to_cmd(func, (filetree, ), {'run': True},
                                  tmp_dir=logdir_path)
                if 'job_name' not in submit_kwargs:
                    submit_kwargs['job_name'] = func.__name__
                job_id = (submit_params(cmd, **submit_kwargs), )
                new_wait_for = job_id
            else:
                if submit_kwargs.get('wait_for', None):
                    logger.warning('{}: waiting for cluster jobs to finish'.format(func.__name__))
                    hold(submit_kwargs.get('wait_for'))
                logger.info('new {}: starting to run locally'.format(func.__name__))
                func(filetree)
                not_finished = tuple(name for name in names
                                     if len(filetree.get_all(name, glob_vars=glob_vars)) == 0)
                if len(not_finished) != 0:
                    raise ValueError(func.__name__ + ': did not produce output for ' + str(not_finished))
                logger.debug('new {}: finished run'.format(func.__name__))
                new_wait_for = ()
            prev_runs[id_prev_run] = new_wait_for
            return new_wait_for

        return wrapper

    return get_wrapper


class Pipeline(object):
    """
    Pipeline function containing one or more scripts

    Usage:

    - First define a new pipeline with a FileTree and all possible parameter values that you want to iterate over
        within the pipeline, e.g.

        .. codeblock:: python

            from fsl.utils.filetree import FileTree
            bet_tree = FileTree("input filename")
            pipe = Pipeline(bet_tree, all_values={'subject': ('A', 'B'), 'modality': ('T1w', 'T2w'), 'fval': [0.5]})

        The filetree might look something like

        .. codeblock::

            {subject}
                {modality}.nii.gz (raw)
                {modality}_f{fval}_brain.nii.gz (brain_extracted)

    - Register individual functions that run part of the pipeline, e.g.:

        .. codeblock:: python

            from fsl import wrappers
            @pipe(in_files=['raw'], out_files=['brain_extracted'], in_vars=['fval'])
            def bet(raw, brain_extracted, fval=0.5):
                wrappers.bet(raw, brain_extracted, f=fval)

        The pipeline will automatically chain these functions together based on their input and output files and
        run them in the required order.

    - Tell the pipeline to run. You can directly call :method:`pipe.run() <run>`, which will run `bet`
        for all requested subjects, modalities and values of `fval`.
        Depending on the provided flags, you can run all the jobs locally in sequence, locally in parallel using dask,
        or submit them to a cluster using `fslsub`.
        You can also choose whether on not to overwrite any already pre-existing intermediate or output files.
        Alternatively :method:`pipe.cli() <cli>` can be used to allow the user to set all of these options from the command line.

    - For more complicated pipelines it might be useful to produce a graph of the dependencies between all the scripts.
        This can be produced using :method:`pipe.get_graph_filenames() <get_graph_filenames>`, which is also available
        from the command line (:method:`pipe.cli() <cli>`).
    """
    def __init__(self, tree: FileTree, all_values, parents=None):
        """
        Creates a new pipeline to generate the files in `tree`

        :param tree: Templates for the input/output filenames
        :param all_values: All possible values that the variables in the `tree` templates can take
        :param parents: Parent pipelines that might produce some of the dependencies
        """
        if parents is None:
            parents = []
        self.tree = tree.partial_fill()
        self.scripts = set()
        self.default_all_values = all_values
        self.parents = parents

    def __call__(self, *args, **kwargs):
        """
        Adds a given function to the pipeline

        Use as a decorator:

        .. codeblock:: python

            pipe = Pipeline(...)

            @pipe(in_files=..., out_files=...)
            def func(...)

        :param in_files: sequence of input short names of templates in FileTree
        :param out_files: sequence of output short names of templates in FileTree
        :param in_vars: sequence of variables names, whose values are required to run the script
        :param possible_values: Dictionary listing for a variable which values it can take (optional)
        :param iter_vars: Iterate over specific variables when producing input/output filenames
        :param allow_missing: allow input filenames to be missing (None will be provided to the function)
        :param submit_params: Any other keywords are used to submit the job to `fsl_sub`
        :return: Used as a decorator the function itself is returned unaltered
        """
        def add_script(func):
            """
            Wrapped function to actually create the script around a function `func`
            """
            func.script = Script(self, func, *args, **kwargs)
            self._add(func.script)
            return func
        return add_script

    def _add(self, script: "Script"):
        """
        Adds a script to the pipeline

        :param script: Wrapper of a function defining input/output filenames
        """
        script.pipeline = self
        self.scripts.add(script)

    def run(self, short_name=None, catch=True, overwrite=True, overwrite_dependencies=False,
            method='single', values=None):
        """
        Generates all files matching the short_name and variables

        :param short_name: sequence of short names to produce
        :param catch: If True catches any errors incurred while running scripts
        :param overwrite: overwrites the requested output files even if they already exist
        :param overwrite_dependencies: overwrites the dependencies of the requested output files even if they already exist
        :param method: Method to submit job. One of 'single' (runs job in sequence), 'dask' (runs job in parallel on single machine), or 'submit' (submits job using `fsl_sub`)
        :param values: Limits the variables to the options listed in this dictionary (overwrites any values set in `all_values`)
        """
        all_values = dict(self.default_all_values)
        if values is not None:
            all_values.update(values)
        for script in self.scripts:
            script.generate_runs(all_values)

        if short_name is None:
            short_name = set()
            for script in self.scripts:
                short_name.update(script.out_files)
        if method == 'dask':
            try:
                from dask.distributed import Client; Client()
            except ImportError as e:
                print(e)
            else:
                import webbrowser
                webbrowser.open("http://localhost:8787/status")
        dask_jobs = []
        valid_run = set()
        invalid_run = set()
        missing_targets = set()
        for values in itertools.product(*list(all_values.values())):
            vars = {name: value for name, value in zip(all_values.keys(), values)}
            var_tree = self.tree.update(**vars)
            for sn in short_name:
                target = self.get_target(var_tree.get(sn))
                if target.producer is None:
                    logger.warning(f"No script found to produce {target}")
                    continue
                if target.filename in valid_run or target.filename in invalid_run:
                    continue
                missing = target.producer.missing(overwrite=overwrite,
                                                  overwrite_dependencies=overwrite_dependencies)
                missing_targets.update(missing)
                if len(missing) > 0 and not target.producer.allow_missing:
                    invalid_run.add(target.filename)
                else:
                    valid_run.add(target.filename)
        if len(invalid_run) > 0:
            self._print_missing_runs(invalid_run, short_name)
        if len(missing_targets) > 0:
            tmp_log = logger.info if method == 'check' else logger.debug
            tmp_log('The following input filenames are missing:\n' +
                    '\n'.join(sorted(t.filename for t in missing_targets)))
        if method == 'check':
            self._print_missing_runs(valid_run, short_name, True)
            return
        for fn in sorted(valid_run):
            target = self.get_target(fn)
            try:
                if method == 'single':
                    target.producer.run(overwrite=overwrite,
                                        overwrite_dependencies=overwrite_dependencies)
                    logger.debug(f"Finished {target}")
                elif method == 'dask':
                    dask_jobs.append(target.producer.as_dask(overwrite=overwrite,
                                                             overwrite_dependencies=overwrite_dependencies,
                                                             catch=catch))
                elif method == 'submit':
                    target.producer.submit(overwrite=overwrite,
                                           overwrite_dependencies=overwrite_dependencies)
                else:
                    raise ValueError(f"Method should be one of 'single', 'dask', 'submit', 'check'; not {method}")
            except Exception as e:
                logger.info(f"Error producing {target}: {e}")
                if not catch:
                    raise e
        if method == 'dask':
            import dask
            dask.compute(*dask_jobs)

    def cli(self, description="Runs the pipeline"):
        """
        Runs the pipeline from the command line

        :param description: Description to give of the script in the help message
        """
        logger.enable("mcutils")
        parser = argparse.ArgumentParser(description)
        templates = set.union(*[script.out_files for script in self.scripts])
        default_method = 'submit' if SUBMIT else 'dask'

        sub_parsers = parser.add_subparsers()
        run_parser = sub_parsers.add_parser("run", help="Runs the pipeline")
        run_parser.add_argument("templates", nargs="*",
                                help=f"For which templates to produce the files (default: {', '.join(templates)})")
        run_parser.add_argument("-m", '--pipeline_method', default=default_method,
                                choices=('single', 'submit', 'dask', 'check'),
                                help=f"method used to run the jobs (default: {default_method})")
        run_parser.add_argument("-o", '--overwrite', action='store_true', help="If set overwrite any requested files")
        run_parser.add_argument("-d", '--overwrite_dependencies', action='store_true',
                                help="If set also overwrites dependencies of requested files")
        run_parser.add_argument("-r", '--raise_errors', action='store_true',
                                help="If set raise errors rather than catching them")
        for var, values in self.default_all_values.items():
            run_parser.add_argument(f"--{var}", nargs='+', default=tuple(values),
                                    help=f"If set restricts the possible values of {var} to the selected values " +
                                         f"(default: {', '.join([str(v) for v in values])})")
        run_parser.set_defaults(func=self._run_args, all_templates=templates)

        graph_parser = sub_parsers.add_parser("graph", help="Graph of pipeline")
        graph_parser.add_argument("-o", "--output", help="Basename where to store the output")
        graph_parser.add_argument("-f", "--filenames", nargs="*",
                                  help="Produce dependency/output graphs for given filenames")
        graph_parser.set_defaults(func=self._graph_args)

        args = parser.parse_args()
        args.func(args)

    def _run_args(self, args):
        """
        Runs the pipeline based on the arguments set from the command line

        Helper function for :method:`cli`
        """
        for var, values in self.default_all_values.items():
            if getattr(args, var, None) is not None:
                unrecognised_values = set(getattr(args, var)).difference(values)
                if len(unrecognised_values) > 0:
                    logger.info(f"Novel values for {var}: {unrecognised_values}")

        update_values = {}
        for var in self.default_all_values.keys():
            if getattr(args, var, None) is not None:
                update_values[var] = getattr(args, var)

        self.run(
            short_name=args.all_templates if len(args.templates) == 0 else args.templates,
            catch=not args.raise_errors,
            overwrite=args.overwrite,
            overwrite_dependencies=args.overwrite_dependencies,
            method=args.pipeline_method,
            values=update_values,
        )

    def _graph_args(self, args):
        """
        Produces graph of the pipeline based on arguments set from the command line

        Helper function for :method:`cli`
        """
        for script in self.scripts:
            script.generate_runs()

        graph = self.get_graph_filenames(args.filenames)
        if args.output is None:
            (_, fn) = mkstemp(suffix='.svg')
            graph.render(fn, view=True)
        else:
            graph.render(args.output)

    def get_target(self, filename: str) -> "Target":
        """
        Finds the target describing how `filename` gets produced

        :param filename: filename as generated by the `FileTree` object
        :return: `Target` object describing how to produce file
        """
        if not hasattr(self, '_targets'):
            self._targets = {}
        if filename not in self._targets:
            self._targets[filename] = Target(filename)
        result = self._targets[filename]
        if result.producer is None:
            for parent in self.parents:
                parent_result = parent.get_target(filename)
                if parent_result.producer is not None:
                    return parent_result
        return result

    def _print_missing_runs(self, filenames, all_short_names, valid_runs=False):
        """
        Prints a summary of all the files that can not be produced due to missing input files
        """
        targets = [self.get_target(fn) for fn in filenames]
        all_scripts: Set[ScriptRun] = set(target.producer for target in targets)
        all_key_values = defaultdict(set)
        for sn in all_scripts:
            for key, value in sn.variables.items():
                all_key_values[key].add(value)

        all_keys = tuple(sorted(all_key_values.keys(), key=lambda k: -len(all_key_values[k])))
        map_to_sn = defaultdict(set)
        for sn in all_scripts:
            values = tuple(str(sn.variables.get(key, '')) for key in all_keys)
            map_to_sn[values].add(sn)

        nspaces = [max(*([len(key)] + [len(value_set[idx]) for value_set in map_to_sn]))
                       for idx, key in enumerate(all_keys)]
        to_format = ' | '.join([f'{{:{n}s}}' for n in nspaces]) + ' | {}'

        if valid_runs:
            lines = ['For following variables all required files are present:']
        else:
            lines = ['For following variables required files are missing:']

        lines.append(to_format.format(*(all_keys + ('template keys (i.e., short names)', ))))

        sorted_values = map_to_sn.keys()
        for idx in range(len(all_keys), 0, -1):
            sorted_values = sorted(sorted_values, key=lambda v: v[idx-1])

        for value_set in sorted_values:
            all_output_keys = set()
            for sn in map_to_sn[value_set]:
                all_output_keys.update(sn.script.out_files)
            requested = all_output_keys.intersection(all_short_names)
            lines.append(to_format.format(*(value_set) + (', '.join(sorted(requested)), )))

        logger.info('\n'.join(lines))

    def get_graph_filenames(self, filenames: Sequence[str]) -> Digraph:
        """
        Produces a graph illustrating the files related to the provded filenames

        Both upstream and downstream dependencies are included.

        :param filenames: filenames required or produced by the pipeline
        :return: graphviz graph showing the dependencies
        """
        graph = Digraph(format='svg')
        for filename in filenames:
            target = self.get_target(filename)
            target.add_to_graph(graph, upwards='both')
        return graph

    def streamlit(self, title="Pipeline"):
        """
        Illustrates the pipeline in a Streamlit app (EXPERIMENTAL)

        Requires streamlit to be installed.
        """
        import streamlit as st
        import base64

        def render_svg(svg):
            """Renders the given svg string."""
            b64 = base64.b64encode(svg.encode('utf-8')).decode("utf-8")
            html = r'<img src="data:image/svg+xml;base64,%s"/>' % b64
            st.write(html, unsafe_allow_html=True)

        templates = {f for script in self.scripts for f in script.in_files.union(script.out_files)}

        st.title(title)
        template = st.selectbox("Filter graph based on template", ['<all>'] + list(templates), index=0)
        variables = set(self.default_all_values.keys())
        if template != '<all>':
            variables = variables.intersection(self.tree.template_variables(variables))
        values = {}
        iter_over = []
        for variable in variables:
            value = st.selectbox(f"Filter graph based on {variable}",
                                 ['<all>'] + list(self.default_all_values[variable]), index=0)
            if value == '<all>':
                iter_over.append(variable)
            else:
                values[variable] = variable
        tree = self.tree.update(**values)
        filenames = []
        for iter_values in itertools.product(*[self.default_all_values[variable] for variable in iter_over]):
            iter_tree = tree.update(**{var: val for var, val in zip(iter_over, iter_values)})
            if template != '<all>':
                filenames.append(iter_tree.get(template))
            else:
                filenames.extend([iter_tree.get(template) for template in templates])
        st.write('Generating graph for', filenames)
        graph = self.get_graph_filenames(filenames)
        svg = graph.pipe(format='svg')
        render_svg(svg)


class Target(object):
    """
    Object to store for each `filename` which `ScriptRun` is used to produce it
    """
    def __init__(self, filename):
        """
        Creates a new `Target` defining how to produce `filename`

        :param filename: Name of the file to be produced (or input filename)
        """
        self.filename = filename
        self._producer = None
        self.required_by = set()
        self._nodes = {}

    @property
    def exists(self, ) -> bool:
        """
        Tests whether file exists on disk

        :return: True if file exists
        """
        if not hasattr(self, "_exists"):
            self._exists = os.path.exists(self.filename)
        return self._exists

    def delete_cache(self):
        if hasattr(self, '_exists'):
            del self._exists

    @property
    def expected(self, ) -> bool:
        """
        Whether file is expected to be produced
        """
        if self.exists:
            return True
        if self.producer is None:
            return False
        if self.producer.allow_missing or len(self.producer.missing(False, False)) == 0:
            return True
        return False

    @property
    def producer(self, ) -> Optional["ScriptRun"]:
        """
        :return: ScriptRun object that needs to be run to produce this file (or None if this is an input file)
        """
        return self._producer

    @producer.setter
    def producer(self, value: "ScriptRun"):
        if self._producer is not None and self._producer != value:
            raise ValueError(f"{self} can be produced by both {self._producer} and {value}")
        self._producer = value

    def __str__(self):
        return f"Target({self.filename})"

    def __repr__(self):
        return str(self)

    def _add_node(self, graph: Digraph):
        """
        Helper function to add the individual nodes
        """
        global _label_counter
        if graph in self._nodes:
            return self._nodes[graph]
        if self.exists:
            color = 'green'
            tooltip = 'File exists!'
        elif self.expected:
            color = 'gray'
            tooltip = 'File will be produced by pipeline'
        else:
            color = 'red'
            if self.producer is None:
                tooltip = 'Non-existent input file'
            else:
                missing = "\n- ".join(sorted([t.filename for t in self.producer.missing()]))
                tooltip = f'Missing essential files to produce this file:\n- {missing}'
        tooltip = f"{self.filename}\n{tooltip}"
        graph.node(str(_label_counter), Path(self.filename).name,
                   color=color, tooltip=tooltip)
        self._nodes[graph] = str(_label_counter)
        _label_counter += 1
        return self._nodes[graph]

    def add_to_graph(self, graph: Digraph, upwards='both'):
        """
        Adds filename to the graph

        :param graph: Graphviz object to add this node to
        :param upwards: if True adds the producer to the graph next, otherwise add the scripts enabled by this file
        """
        if graph in self._nodes:
            return self._nodes[graph]
        node_id = self._add_node(graph)
        if upwards or upwards == 'both':
            if self.producer is not None:
                producer_id = self.producer.add_to_graph(graph, upwards=True)
                graph.edge(producer_id, node_id)
        if not upwards or upwards == 'both':
            for script_run in self.required_by:
                next_id = script_run.add_to_graph(graph, upwards=False)
                graph.edge(node_id, next_id)
        return node_id


class Script(object):
    def __init__(self, pipeline: Pipeline, func, in_files, out_files, in_vars=None, possible_values=None,
                 iter_vars=None, allow_missing=False, **submit_params):
        """
        Creates a new script.

        Typically not called directly, but by using :class:`Pipeline` object as a decorator

        :param pipeline: Pipeline object of which this script is a part
        :param func: function to run when calling script
        :param in_files: sequence of input short names of templates in FileTree
        :param out_files: sequence of output short names of templates in FileTree
        :param in_vars: sequence of variables names, whose values are required to run the script
        :param possible_values: Dictionary listing for a variable which values it can take (optional)
        :param iter_vars: Iterate over specific variables when producing input/output filenames
        :param allow_missing: allow input filenames to be missing (None will be provided to the function)
        :param submit_params: Any parameters used to submit the job to `fsl_sub`
        """
        self.submit_params = SubmitParams(**submit_params)

        if possible_values is None:
            possible_values = {}
        if in_vars is None:
            in_vars = set()
        if iter_vars is None:
            iter_vars = ()
        self.pipeline = pipeline
        self.func = func
        self.signature = inspect.signature(self.func)
        self.in_files = set(in_files)
        self.in_vars = set(in_vars)
        self.out_files = set(out_files)
        self.possible_values = possible_values
        self.has_run = {}
        self.iter_vars = tuple(iter_vars)
        self.allow_missing = allow_missing

        self.required_variables = self.in_vars.union(*[self.tree.template_variables(sn, optional=False) for sn in self.all_files])
        self.optional_variables = set.union(*[self.tree.template_variables(sn) for sn in self.all_files]).difference(self.required_variables)

        diff = set()
        for template in self.in_files.union(self.out_files):
            try:
                self.tree.get_template(template)
            except KeyError:
                diff.add(template)
        if len(diff) > 0:
            raise KeyError(f"Input/output templates {diff} not available in FileTree")
        for sn in self.out_files:
            diff = set(self.axes).difference(self.tree.template_variables(sn))
            if len(diff) > 0:
                raise ValueError(f"{self}: Output template {sn} (= {self.tree.get_template(sn)[0]}) has no keys for {diff}")

    def generate_runs(self, all_values=None):
        """
        Generates all runs for this script

        :param all_values: dictionary giving for each parameter all the relevant values
        """
        if all_values is None:
            all_values = self.pipeline.default_all_values

        def get_values(name):
            if name in self.possible_values:
                return list(set.intersection(all_values[name], self.possible_values[name]))
            else:
                return all_values[name]

        iter_vars = {}
        for idx, to_iter in enumerate(self.iter_vars):
            shape = [1] * len(self.iter_vars)
            shape[idx] = -1
            iter_vars[to_iter] = np.asarray(get_values(to_iter)).reshape(shape)

        axes_values = [get_values(name) for name in self.axes]
        runs = np.zeros([len(v) for v in axes_values], dtype='object')

        if len(axes_values) == 0:
            runs[()] = ScriptRun(self, {}, iter_vars)
        else:
            for values_obj in itertools.product(*[enumerate(values) for values in axes_values]):
                idx, values = zip(*values_obj)
                runs[idx] = ScriptRun(self, {name: value for name, value in zip(self.axes, values)}, iter_vars)

    @property
    def tree(self, ) -> FileTree:
        """
        FileTree object from the pipeline
        """
        return self.pipeline.tree

    @property
    def all_files(self, ) -> Set[str]:
        """
        All the input and output template names
        """
        return set.union(self.in_files, self.out_files)

    @property
    def all_variables(self, ):
        """
        All the required and optional variable names
        """
        return set.union(self.required_variables, self.optional_variables)

    @property
    def axes(self, ):
        """
        Sorted list of along which variables the script can be run in parallel
        """
        return tuple(sorted(self.all_variables.difference(self.iter_vars)))

    def __call__(self, *args, **kwargs):
        """
        Calls the function directly with the given arguments (ignoring any pipeline stuff)
        """
        return self.func(*args, **kwargs)

    def __repr__(self):
        return f"Script({self.func.__name__})"


class ScriptRun(object):
    """
    Single call of the script with a set of variables
    """
    def __init__(self, script: Script, variables, iter_vars):
        """
        Represents a single potential run of the `Script`

        :param script: parent script
        :param variables: variable values to use when running the script
        :param iter_vars: variables to iterate over, maps N variables to N-dimensional array that are broadcastable
        """
        self.script = script
        self.variables = variables
        self.iter_vars = iter_vars
        self.tree = self.script.tree.update(**self.variables)
        assert tuple(sorted(variables.keys())) == self.script.axes

        self._already_ran = False

        self.provides = {}
        self.requires = {}

        get_target = np.vectorize(self.script.pipeline.get_target, otypes=['object'])
        for output_dict, short_names in [(self.provides, self.script.out_files),
                                         (self.requires, self.script.in_files)]:
            for short_name in short_names:
                filenames = iter_filenames(self.tree, short_name, iter_values=self.iter_vars)
                targets = get_target(filenames)
                output_dict[short_name] = targets.item() if targets.ndim == 0 else targets

        for target in self.requires_iter():
            target.required_by.add(self)
        for target in self.provides_iter():
            target.producer = self

        self._nodes = {}

    def provides_iter(self):
        for values in self.provides.values():
            if isinstance(values, Target):
                yield values
            else:
                yield from values.flat

    def requires_iter(self):
        for values in self.requires.values():
            if isinstance(values, Target):
                yield values
            else:
                yield from values.flat

    @property
    def allow_missing(self, ):
        return self.script.allow_missing

    def output_exists(self, error=False) -> bool:
        """
        Checks whether all output files already exist on disk

        :param error: if True raise an error rather than just returning True or False
        :return: False if any output file is missing
        """
        missing = set()
        for target in self.provides_iter():
            if not target.exists:
                if not error:
                    return False
                missing.add(target.filename)
        if error and len(missing) > 0:
            raise IOError(f"Following output files are missing: {missing}")
        return True

    def input_exists(self, error=False) -> bool:
        """
        Checks whether all input files already exist on disk

        :param error: if True raise an error rather than just returning True or False
        :return: False if any input file is missing
        """
        missing = set()
        for target in self.requires_iter():
            if not target.exists and (
                not self.allow_missing or target.producer is None or len(target.producer.missing()) == 0
            ):
                if not error:
                    return False
                missing.add(target.filename)
        if error and len(missing) > 0:
            raise IOError(f"Following input files are missing: {missing}")
        return True

    def make_output_directories(self, ):
        for target in self.provides_iter():
            Path(target.filename).parents[0].mkdir(parents=True, exist_ok=True)

    def get_kwargs(self, ):
        """
        :return: Keyword arguments needed to provide all the filenames & variable values when calling script
        """
        kwargs = {var: self.iter_vars[var] if var in self.script.iter_vars else self.tree.get_variable(var)
                  for var in self.script.in_vars}
        for is_output, target_dict in enumerate((self.requires, self.provides)):
            get_fn = np.vectorize(lambda t: t.filename if (is_output == 1) or t.expected else None, otypes=['object'])
            for name, targets in target_dict.items():
                part_name = name.split('/')[-1]
                if isinstance(targets, Target):
                    kwargs[part_name] = targets.filename if (is_output == 1) or targets.expected else None
                else:
                    kwargs[part_name] = get_fn(targets)
        return kwargs

    def dependencies(self, ) -> Set["ScriptRun"]:
        """
        Find all the dependencies

        :return: Scripts to run to produce all dependencies
        """
        return set(target.producer for target in self.requires_iter() if target.producer is not None)

    @lru_cache(maxsize=None)
    def missing(self, overwrite=False, overwrite_dependencies=False):
        """
        Finds any missing input files that will prevent this script from running

        :param overwrite: whether to run the script even if output already exists
        :param overwrite_dependencies: whether to run dependencies even if their output already exists
        :return: the targets missing to run this sript
        """
        if self.allow_missing:
            return set()
        if not overwrite and self.output_exists():
            return set()
        missing = set()
        for target in self.requires_iter():
            if target.producer is None:
                if not target.exists:
                    missing.add(target)
            else:
                if overwrite_dependencies or not target.exists:
                    missing.update(target.producer.missing())
        return missing

    @property
    def expected(self, ):
        return self.allow_missing or len(self.missing()) == 0

    def run(self, overwrite=False, overwrite_dependencies=False):
        """
        Runs this script and its dependencies in sequence

        :param overwrite: whether to run the script even if output already exists
        :param overwrite_dependencies: whether to run dependencies even if their output already exists
        """
        if self._already_ran:
            return
        if not overwrite and self.output_exists():
            logger.debug(f'{self}: all output files already exist')
            return
        if not self.expected:
            missing = self.missing(overwrite, overwrite_dependencies)
            raise DependencyError(f"{self}: Required input files are missing: {missing}")
        for dependency in self.dependencies():
            try:
                dependency.run(overwrite_dependencies, overwrite_dependencies)
            except DependencyError:
                if not self.allow_missing:
                    raise
        logger.debug(f'{self} produces missing files; {", ".join([t.filename for t in self.provides_iter() if not t.exists])}')
        self._already_ran = True
        logger.info(f"Running script {self}")
        self(overwrite=overwrite)

    def submit(self, overwrite=False, overwrite_dependencies=False) -> Optional[str]:
        """
        Submits this script and its dependencies to the cluster

        :param overwrite: whether to submit the script even if output already exists
        :param overwrite_dependencies: whether to submit dependencies even if their output already exists
        :return: string with job ID or None if no job was submitted
        """
        if hasattr(self, '_job'):
            return self._job
        if not overwrite and self.output_exists():
            logger.debug(f'{self}: all output files already exist')
            self._job = None
        elif not self.expected:
            missing = self.missing(overwrite, overwrite_dependencies)
            logger.debug(f"Required input files are missing: {missing}")
            self._job = None
        else:
            wait_for = set()
            for dependency in self.dependencies():
                dependency_job = dependency.submit(overwrite_dependencies, overwrite_dependencies)
                if dependency_job is not None:
                    wait_for.add(dependency_job)
            logger.info(f"Submitting new {self} to cluster after {wait_for}")
            logdir = self.script.submit_params.logdir
            logdir_path = None if logdir is None else self.tree.get(logdir, make_dir=True)
            if logdir_path is not None and not os.path.isdir(logdir_path):
                Path(logdir_path).mkdir(parents=True, exist_ok=True)
            self.make_output_directories()
            cmd = func_to_cmd(self.script.func, (), self.get_kwargs(),
                              tmp_dir=logdir_path)
            job_name = self.script.func.__name__ if self.script.submit_params.job_name is None else self.script.submit_params.job_name
            to_submit = copy(self.script.submit_params)
            to_submit.logdir = logdir_path
            job_id = self.script.submit_params(cmd, wait_for=wait_for, job_name=job_name, logdir=logdir_path)
            self._job = job_id
        return self._job

    def as_dask(self, overwrite=False, overwrite_dependencies=False, catch=False):
        """
        Schedules this script and its dependencies on dask

        :param overwrite: whether to submit the script even if output already exists
        :param overwrite_dependencies: whether to submit dependencies even if their output already exists
        :param catch: if True catches any error
        :return: dask Delayed job object
        """
        import dask
        if not hasattr(self, '_dask_job'):
            if not overwrite and self.output_exists():
                self._dask_job = dask.delayed(lambda: 0, name='placeholder')
            elif not self.expected:
                missing = self.missing(overwrite, overwrite_dependencies)
                logger.debug(f"Required input files are missing: {missing}")
                self._dask_job = dask.delayed(lambda: 0, name='placeholder')
            else:
                def dask_job(*args):
                    if any(a != 0 for a in args):
                        logger.debug(f"{self} skipped because dependencies failed")
                        return 1

                    logger.debug(f"Running {self} using dask")
                    try:
                        self(overwrite=overwrite)
                    except Exception as e:
                        if catch:
                            logger.exception(f"{self} failed: {e}")
                            return 1
                        else:
                            logger.info(f"{self} failed: {e}")
                            raise e
                    return 0
                self._dask_job = dask.delayed(dask_job, name=str(self))(
                    *[dep.as_dask(overwrite_dependencies, overwrite_dependencies, catch)
                      for dep in self.dependencies()]
                )
        return self._dask_job

    def __call__(self, overwrite=False):
        """
        Calls the script with the input/ouput filenames and variables for this run

        Does not check dependencies. To run dependencies as well check :meth:`run`, :meth:`submit`, or :meth:`as_dask`.

        :param overwrite: whether to run the script even if output already exists
        :return: return of the script call
        """
        if not overwrite and self.output_exists():
            raise ValueError(f"Output files for {self} already exist. Skipping the run")
        for target in self.requires_iter():
            target.delete_cache()
        self.input_exists(error=True)
        self.make_output_directories()
        res = self.script(**self.get_kwargs())
        for target in self.provides_iter():
            target.delete_cache()
        self.output_exists(error=True)
        return res

    def __repr__(self):
        return f"ScriptRun({self.script.func.__name__}, {self.variables})"

    def _add_node(self, graph: Digraph):
        """
        Adds ScriptRun to the graph
        """
        global _label_counter
        if graph in self._nodes:
            return self._nodes[graph]
        if not self.allow_missing and len(self.missing()) > 0:
            color = 'red'
            missing = "\n- ".join(sorted([t.filename for t in self.missing()]))
            tooltip = f'Missing input files:\n- {missing}'
        elif self.output_exists():
            color = 'blue'
            tooltip = f'Produced all output files'
        elif self.input_exists():
            output_exists = [t.filename for t in self.provides_iter() if t.exists]
            if len(output_exists) == 0:
                color = 'green'
                tooltip = f'Ready to run'
            else:
                color = 'darkgreen'
                tooltip = f'Ready to run (some output files exist)'
        else:
            color = 'gray'
            tooltip = 'Waiting on dependencies'
        tooltip = f"""{tooltip}

        existing/expected/total
"""
        for is_output, targets in enumerate([self.requires_iter(), self.provides_iter()]):
            targets = list(targets)
            existing = len([t for t in targets if t.exists])
            expected = len([t for t in targets if t.expected])
            tooltip = tooltip + f"{'output:' if is_output else 'input'} {existing}/{expected}/{len(targets)}\n"

        graph.node(str(_label_counter), self.script.func.__name__,
                   color=color, tooltip=tooltip, shape='box')
        self._nodes[graph] = str(_label_counter)
        _label_counter += 1
        return self._nodes[graph]

    def add_to_graph(self, graph: Digraph, upwards=True):
        """
        Adds ScriptRun to the graph

        :param graph: Graphviz object to add this node to
        :param upwards: if True required filenames next, otherwise add produced filenames
        """
        if graph in self._nodes:
            return self._nodes[graph]
        node_id = self._add_node(graph)
        if upwards:
            for target in self.requires_iter():
                required_node = target.add_to_graph(graph, upwards)
                graph.edge(required_node, node_id)
        else:
            for target in self.provides_iter():
                provides_node = target.add_to_graph(graph, upwards)
                graph.edge(node_id, provides_node)
        return node_id


@dataclass
class SubmitParams(object):
    """
    DEPRECATED: use the version of this in fslpy (`fsl.utils.fslsub.SubmitParams`)

    Represents the fsl_sub parameters

    Any command line script can be submitted by the parameters by calling the `SubmitParams` object:

    .. codeblock:: python

        submit = SubmitParams(minutes=1, logdir='log', wait_for=['108023', '108019'])
        submit('echo finished')

    This will run "echo finished" with a maximum runtime of 1 minute after the jobs with IDs 108023 and 108019 are finished.
    It is the equivalent of

    .. codeblock:: bash

        fsl_sub -T 1 -l log -j 108023,108019 "echo finished"

    For python scripts that submit themselves to the cluster, it might be useful to give the user some control
    over at least some of the submission parameters. This can be done using:

    .. codeblock:: python

        import argparse
        parser = argparse.ArgumentParser("my script doing awesome stuff")
        parser.add_argument("input_file")
        parser.add_argument("output_file")
        SubmitParams.add_to_parser(parser, include=('wait_for', 'logdir'))
        args = parser.parse_args()

        submitter = SubmitParams.from_args(args).update(minutes=10)
        from fsl import wrappers
        wrappers.bet(input_file, output_file, fslsub=submitter)

    This submits a BET job using the -j and -l flags set by the user and a maximum time of 10 minutes.
    """
    minutes: Optional[float] = None
    queue: Optional[str] = None
    architecture: Optional[str] = None
    priority: Optional[int] = None
    email: Optional[str] = None
    wait_for: Union[str, None, Collection[str]] = None
    job_name: Optional[str] = None
    ram: Optional[int] = None
    logdir: Optional[str] = None
    mail_options: Optional[str] = None
    flags: Optional[str] = None
    verbose: bool = False
    environment: dict = None

    cmd_line_flags = {
        '-T': 'minutes',
        '-q': 'queue',
        '-a': 'architecture',
        '-p': 'priority',
        '-M': 'email',
        '-N': 'job_name',
        '-R': 'ram',
        '-l': 'logdir',
        '-m': 'mail_options',
        '-F': 'flags',
    }

    def __post_init__(self):
        """
        If not set explicitly by the user don't alter the environment in which the script will be submitted
        """
        if self.environment is None:
            self.environment = {}

    def as_flags(self, ):
        """
        Returns any parameters that have been explicitly set as a tuple

        This tuple is actually passed on to the command line call of `fsl_sub`
        """
        res = []
        for key, value in self.cmd_line_flags.items():
            if getattr(self, value) is not None:
                res.extend((key, str(getattr(self, value))))
        if self.verbose:
            res.append('-v')
        if self.wait_for is not None and len(_flatten_job_ids(self.wait_for)) > 0:
            res.extend(('-j', _flatten_job_ids(self.wait_for)))
        return tuple(res)

    def __str__(self):
        return f'SubmitParams({" ".join(self.as_flags())})'

    def __repr__(self):
        return str(self)

    def __call__(self, cmd, **kwargs):
        """
        Submits the command using `fsl_sub`

        :param cmd: string or sequence of strings with the command line arguments that should be submitted
        :param kwargs: can be used to override any of the `fsl_sub` flags
        :return: string with the submitted job ID
        """
        runner = self.update(**kwargs)
        if isinstance(cmd, str):
            cmd = cmd.split()
        logger.debug(' '.join(('fsl_sub', ) + tuple(runner.as_flags()) + tuple(cmd)))
        env = dict(os.environ)
        env.update(runner.environment)
        jobid = run(
                ('fsl_sub', ) + tuple(runner.as_flags()) + tuple(cmd),
                stdout=PIPE, check=True, env=env,
        ).stdout.decode().strip()
        logger.debug(f'Job submitted as {jobid}')
        return jobid

    def update(self, **kwargs):
        """
        Creates a new SubmitParams withe updated parameters
        """
        values = asdict(self)
        values.update(kwargs)
        return SubmitParams(**values)

    @classmethod
    def add_to_parser(cls, parser: argparse.ArgumentParser, as_group='fsl_sub commands', skip=()):
        """
        Adds submission parameters to the parser

        :param parser: parser that should understand submission commands
        :param as_group: add as a new group
        :param skip: sequence of argument flags/names that should not be added to the parser
        :return: the group the arguments got added to
        """
        try:
            fsl_sub_run = run(['fsl_sub'], stdout=PIPE)
        except FileNotFoundError:
            warnings.warn('fsl_sub was not found')
            return
        doc_lines = fsl_sub_run.stdout.decode().splitlines()
        nspaces = 1
        for line in doc_lines:
            if len(line.strip()) > 0:
                while line.startswith(' ' * nspaces):
                    nspaces += 1
        nspaces -= 1
        if as_group:
            group = parser.add_argument_group(as_group)
        else:
            group = parser
        for flag, value in cls.cmd_line_flags.items():
            if value in skip or flag in skip:
                continue
            explanation = None
            for line in doc_lines:
                if explanation is not None and len(line.strip()) > 0 and line.strip()[0] != '-':
                    explanation.append(line[nspaces:].strip())
                elif explanation is not None:
                    break
                elif line.strip().startswith(flag):
                    explanation = [line[nspaces:].strip()]
            explanation = ' '.join(explanation)
            as_type = {'minutes': float, 'priority': int, 'ram': int, 'verbose': None}
            action = 'store_true' if value == 'verbose' else 'store'
            group.add_argument(flag, dest='sub_' + value, help=explanation, action=action,
                               type=as_type[value] if value in as_type else str)
        return group

    @classmethod
    def from_args(cls, args):
        """
        Create a SubmitParams from the command line arguments
        """
        as_dict = {value: getattr(args, 'sub_' + value, None) for value in cls.cmd_line_flags.values()}
        return cls(**as_dict)


def iter_filenames(tree: FileTree, short_name, iter_values) -> Union[str, np.ndarray]:
    """
    Iterate over all possible filenames for given `short_name`

    :param tree: Input file tree
    :param short_name: name of the template in `self.tree`
    :param iter_values: values to iterate over
    :return: Returns filenames or array of filenames (if `template` contains one of the `iter_vars`)
    """
    if iter_values is None:
        iter_values = {}
    if len(iter_values) == 0:
        fn = tree.get(short_name)
        return fn
    in_template = tree.template_variables(short_name)
    to_iter = sorted(in_template.intersection(iter_values.keys()))

    def get_fn(*var_value):
        kwargs = {name: value for name, value in zip(to_iter, var_value)}
        fn = tree.update(**kwargs).get(short_name)
        return fn

    input_args = [iter_values[name] for name in to_iter]
    return np.vectorize(get_fn, otypes=['object'])(*input_args)
