"""
This package contains a wide variety of utilities and scripts.

After installation the :mod:`scripts` will be available by running `mc_script`.
On jalapeno the latest version is available without installation using `~ndcn0236/bin/mc_script`.

Many python utilities are also available.
"""

__version__ = '0.1.0'
__author__ = 'Michiel Cottaar <MichielCottaar@protonmail.com>'
__all__ = []


from fsl.utils.filetree import tree_directories
import os.path as op
tree_directories.append(
        op.join(op.split(__file__)[0], 'trees')
)
del tree_directories, op