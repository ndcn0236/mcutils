"""
Registers multiple images to each other

The images are passed around as a dictionary mapping identifier to filename or short name
"""
from fsl.utils.filetree import FileTree
from mcutils.pipe import generates
from subprocess import run
from loguru import logger
import os


def resolve(tree: FileTree, filename):
    """
    Resolves a filename or short name

    :param tree: input/output filenames
    :param filename: filename or template key in tree
    :return: actual filename
    """
    try:
        return tree.get(filename)
    except KeyError:
        return filename


@generates(['init_mean'], queue='short.q', logdir='log')
def initialize(tree):
    """
    Computes the initial mean before any registration

    :param tree: input/output filenames
    """
    images = tree.images
    run(
            ['fsladd', tree.get('init_mean', make_dir=True), '-m'] +
            [resolve(tree, fn) for fn in images.values()]
    )


@generates(['iter_linear', 'iter_transformed'], queue='short.q', logdir='log')
def fit_linear(tree: FileTree):
    """
    Iterates linear registration

    :param tree: input/output filenames
    """
    images = tree.images
    fn = resolve(tree, images[tree.get_variable('identifier')])
    iter = tree.get_variable('iter')
    if iter == 1:
        mean_fn = tree.get('init_mean')
        init_trans = []
    else:
        mean_fn = tree.update(iter=iter - 1).get('iter_mean')
        init_trans = ['-init', tree.update(iter=iter - 1).get('iter_linear')]
    run([
        'flirt',
        '-in', fn,
        '-ref', mean_fn,
        '-out', tree.get('iter_transformed', make_dir=True),
        '-omat', tree.get('iter_linear', make_dir=True)
    ] + init_trans + list(tree.flags))


@generates(['iter_mean'], queue='short.q', logdir='log')
def get_mean(tree: FileTree):
    """
    Iterates a linear registration to the previous mean using flirt

    :param tree: input/output filenames
    """
    run(
            ['fsladd', tree.get('iter_mean', make_dir=True), '-m'] +
            [tree.update(identifier=identifier).get('iter_transformed') for identifier in tree.images]
    )


@generates([], queue='veryshort.q', logdir='log')
def clean_transformed(tree: FileTree):
    """
    Iterates a linear registration to the previous mean using flirt

    :param tree: input/output filenames
    :param images: mapping from image identifier to filename or tree short name
    """
    for identifier in tree.images:
        fn = tree.update(identifier=identifier).get('iter_transformed')
        if os.path.exists(fn):
            os.remove(fn)


def multi_reg(tree: FileTree, images, iter=3, wait_for=(), flags=()):
    """
    Register multiple images to each other

    :param tree: input/output filenames
    :param images: mapping from image identifier to filename (or template key in tree)
    :param iter: number of iterations
    :param wait_for: jobs to wait for before registration
    :param flags: sequence of flags to add to the FLIRT call
    :return: tuple of job ids that job dependent on the registration should wait for
    """
    tree.exists(['final_dir', 'mean', 'transformed', 'iter_mean', 'iter_transformed', 'init_mean'], error=True)
    final_dir = tree.get('final_dir', make_dir=True)
    if os.path.islink(final_dir):
        os.remove(final_dir)
    elif os.path.isdir(final_dir):
        os.rmdir(final_dir)
    os.symlink(f'iter_{iter:03d}', final_dir)
    if tree.exists(['mean', 'transformed', 'linear'], on_disk=True, glob_vars=('identifier', )):
        logger.info(f'Final registration output already exists')
        return wait_for

    tree = tree.update()
    tree.images = images
    tree.flags = flags
    latest_job = initialize(tree, wait_for=wait_for)
    for current in range(1, iter + 1):
        if current != iter and tree.update(iter=current).exists(['iter_mean'], on_disk=True):
            logger.debug(f'Output file {tree.update(iter=current).get("iter_mean")} already exists')
            continue
        fit_jobs = ()
        for identifier in images:
            fit_jobs = fit_jobs + fit_linear(tree.update(identifier=identifier, iter=current), wait_for=latest_job)
        latest_job = get_mean(tree.update(iter=current), wait_for=fit_jobs)
        if iter != current:
            clean_transformed(tree.update(iter=current), wait_for=latest_job)
    return latest_job
