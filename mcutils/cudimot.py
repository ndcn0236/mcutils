"""
Manages filetrees for a CUDIMOT fit of the data
"""

from fsl.utils.filetree import FileTree
from fsl.utils.fslsub import func_to_cmd
import os.path as op
from subprocess import run
import os
from fsl.data.image import removeExt
import re
from loguru import logger
import nibabel as nib
from .pipe import SubmitParams


models = {
    'Ball_1_Racket': ('diffusion', 'racket_fraction', 'k1', 'k2', 'theta', 'phi', 'psi'),
    'Ball_1_Stick': ('S0', 'diffusion', 'stick_fraction', 'theta', 'phi'),
    'Ball_1_Stick_Multiexponential': ('S0', 'diffusion', 'diffusion_std', 'stick_fraction', 'theta', 'phi'),
    'Ball_2_Sticks': ('S0', 'diffusion', 'stick1_fraction', 'theta1', 'phi1',
                      'stick2_fraction', 'theta2', 'phi2'),
    'Ball_2_Sticks_Multiexponential': ('S0', 'diffusion', 'diffusion_std',
                                       'stick1_fraction', 'theta1', 'phi1',
                                       'stick2_fraction', 'theta2', 'phi2'),
    'DiffusionTensor': ('S0', 'Dxx', 'Dxy', 'Dxz', 'Dyy', 'Dyz', 'Dzz'),
    'NODDI_Bingham': ('free_water_fraction', 'intra_fraction', 'kappa', 'beta', 'theta', 'phi', 'psi'),
    'NODDI_Watson': ('free_water_fraction', 'intra_fraction', 'kappa', 'theta', 'phi'),
}


def file_or_short_name(tree: FileTree, name, check=True):
    """
    Returns full filename from tree

    :param tree: FileTree
    :param name: full filename or short name in tree
    :param check: if True raises an error if the file does not exist
    :return: full filename
    """
    if tree.defines([name]):
        name = tree.get(name)
    if check and not op.exists(name):
        raise ValueError(f"File {name} does not exist")
    return name


def cudimot(directory_or_tree, data_file, mask_file, cfp=(), fixp=(),
            model_name=None, njobs=4, cudimot_dir=None, wait_for=(),
            overwrite=True, optional_flags=''):
    """
    Runs or submits the job to fit the given model

    :param directory_or_tree: directory that will contain the output files or tree with CUDIMOT input/output files
    :param data_file: path to the data file (or short name in tree)
    :param mask_file: path to the mask file (or short name in tree)
    :param cfp: sequence of filenames or short names with common fixed parameters
    :param fixp: sequence of filenames or short names with voxel-wise fixed parameters
    :param model_name: which model to fit (defaults to tree.variables['model_name'])
    :param njobs: number of jobs to split the data into
    :param cudimot_dir: directory where CUDIMOT has been installed (defaults to $FSLDEVDIR)
    :param wait_for: tuple of job ids that cudimot should wait for
    :param overwrite: overwrites the output even if it already exists
    :param optional_flags: string with all optional flags
    :return: tuple with job ids to wait for
    """
    if isinstance(directory_or_tree, FileTree):
        tree = directory_or_tree
    else:
        tree = FileTree.read('cudimot.tree', directory_or_tree)
    tree = tree.update(
            njobs=njobs,
            cudimot_dir=os.environ.get('FSLDEVDIR') if cudimot_dir is None else cudimot_dir
    )
    if model_name is not None:
        tree = tree.update(model_name=model_name)
    model_name = tree.get_variable('model_name')
    data_file = file_or_short_name(tree, data_file, check=len(wait_for) == 0)
    mask_file = file_or_short_name(tree, mask_file, check=len(wait_for) == 0)
    cfp = tuple(file_or_short_name(tree, fn, check=len(wait_for) == 0) for fn in cfp)
    fixp = tuple(file_or_short_name(tree, fn, check=len(wait_for) == 0) for fn in fixp)

    if not overwrite and tree.on_disk(['mean'], glob_vars=['param_name']):
        logger.info(f"cuDIMOT output files already exists")
        return wait_for

    if len(cfp) > 0:
        with open(tree.get('CFP', make_dir=True), 'w') as f:
            for filename in cfp:
                f.write(filename + '\n')

    if len(fixp) > 0:
        with open(tree.get('FixP', make_dir=True), 'w') as f:
            for filename in fixp:
                f.write(filename + '\n')

    if not op.exists(op.join(tree.get_variable('cudimot_dir'), 'bin', model_name)):
        raise ValueError(f'Model {model_name} not found; has it been compiled already?')

    tree.input_files = (data_file, mask_file)

    logger.info(f'Running/submitting CUDIMOT {model_name} fit with ' +
                f'flags set to {required_flags(tree, "command")} {optional_flags}')
    log_dir = tree.get("log", make_dir=True)
    if not op.isdir(log_dir):
        os.mkdir(log_dir)
    logger.debug(f'Logging written to {log_dir}')

    for direc_name in ('parts_dir', 'output_dir'):
        direc = tree.get(direc_name, make_dir=True)
        if not op.isdir(direc):
            os.mkdir(direc)

    split_job = SubmitParams(queue='cuda.q', logdir=log_dir, wait_for=wait_for)(
        f'{tree.get_variable("cudimot_dir")}/bin/split_parts_{tree.get_variable("model_name")} ' +
        required_flags(tree, 'split') + f' {optional_flags}'
    )

    run_jobs = set()
    for part in range(tree.get_variable("njobs")):
        run_jobs.add(SubmitParams(queue='cuda.q', logdir=log_dir, wait_for=split_job)(
            f'{tree.get_variable("cudimot_dir")}/bin/{tree.get_variable("model_name")} ' +
            required_flags(tree.update(part=part), f'run{part}') + f' {optional_flags}'
        ))
    merge_job = SubmitParams(queue='cuda.q', logdir=log_dir, wait_for=tuple(run_jobs))(
        f'{tree.get_variable("cudimot_dir")}/bin/merge_parts_{tree.get_variable("model_name")} ' +
        required_flags(tree, 'merge') + f' {optional_flags}'
    )

    post_job = SubmitParams(queue='short.q', logdir=log_dir, wait_for=merge_job, job_name='post_cudimot')(
        func_to_cmd(post_cudimot, (tree, ), {}, tmp_dir=log_dir)
    )
    return post_job


run._compiling = {}


def required_flags(tree, cudimot_command):
    """
    Get the cudimot flags based on the tree

    :param tree: input/output filenames
    :return: tuple of flags to the CUDIMOT binaries
    """
    return ' '.join([flag for flag in (
        f"--data={tree.input_files[0]}",
        f"--maskfile={tree.input_files[1]}",
        f"--partsdir={tree.get('parts_dir')}",
        f"--outputdir={tree.get('output_dir')}",
        f"--idPart={tree.get_variable('part', 0)}",
        f"--nParts={tree.get_variable('njobs')}",
        f"--CFP={tree.get('CFP')}" if op.exists(tree.get('CFP')) else '',
        f"--FixP={tree.get('FixP')}" if op.exists(tree.get('FixP')) else '',
        f"--ld={tree.get('log')}/{cudimot_command}",
    ) if len(flag) > 0])


def has_mcmc(tree: FileTree):
    """
    Checks whether MCMC was run

    :param tree: cuDIMOT output filenames
    :return: True if samples are available
    """
    for filename in tree.get_all('merged_params', glob_vars=['param_idx']):
        img = nib.load(filename)
        if len(img.shape) > 3:
            return True
    return False


def post_cudimot(tree: FileTree):
    """
    Links the cudimot output to give it more helpful names

    :param tree: cuDIMOT output filenames
    """
    mcmc = has_mcmc(tree)
    target = 'samples' if mcmc else 'mean'
    param_names = get_param_names(tree.get_variable('model_name'))

    for idx in range(len(param_names)):
        if not tree.update(param_idx=idx).on_disk(['merged_params']):
            raise ValueError(f"Missing output CUDIMOT file with index {idx}")

    for idx, name in enumerate(param_names):
        tree_param = tree.update(param_name=name)
        run(['ln', '-s', f'merged/Param_{idx}_samples.nii.gz', tree_param.get(target, make_dir=True)])
        if mcmc:
            run([
                'fslmaths', tree_param.get('samples'), '-Tmean', tree_param.get('mean', make_dir=True)
            ], check=True)
            run([
                'fslmaths', tree_param.get('samples'), '-Tstd', tree_param.get('std', make_dir=True)
            ], check=True)

    for (theta, phi), dyad in get_dyad_names(param_names).items():
        run([
            'make_dyadic_vectors',
            tree.update(param_name=theta).get(target),
            tree.update(param_name=phi).get(target),
            tree.input_files[1],
            removeExt(tree.update(param_name=dyad).get('mean', make_dir=True)),
        ] + (['50'] if mcmc else []), check=True)


def get_dyad_names(param_names):
    """
    Find matching pairs of theta and phi and return the corresponding dyad

    :param param_names: all parameters names
    :return: mapping of (theta name, phi name) -> dyad name
    """
    theta_names = []
    phi_names = []
    for name in param_names:
        match = re.match('(.*)theta(.*)', name)
        if match is not None:
            theta_names.append(match.groups())
        match = re.match('(.*)phi(.*)', name)
        if match is not None:
            phi_names.append(match.groups())
    matches = (parts for parts in theta_names if parts in phi_names)
    return {(pre + 'theta' + post, pre + 'phi' + post):
                pre + 'dyad' + post for pre, post in matches}


def get_param_names(model_name, ):
    """
    Retrieves the parameter names for known Cudimot models

    :param model_name: known Cudimot model
    :return: tuple of parameter names
    """
    if model_name in models:
        return models[model_name]
    match = re.match('Bingham_(\d+)_cross_(\d+)_var', model_name)
    if match is not None:
        # Each Bingham distribution is described by its orientation(P[0], P[1], P[2])
        # and its log - amplitude(P[3] and its derivative(P[4] - P[4 + nparams]) as well as k1 and k2 and their derivatives
        param_names = []
        n_bingham, n_der = [int(value) for value in match.groups()]
        for idx_bingham in range(1, n_bingham + 1):
            param_names.extend([f'Peak{idx_bingham}_{name}' for name in ('theta', 'phi', 'psi')])
            for var_name in ('lA', 'k1', 'k2'):
                param_names.append(f'Peak{idx_bingham}_{var_name}')
                param_names.extend([f'Peak{idx_bingham}_d{var_name}_dvar{idx_der}' for idx_der in range(1, n_der + 1)])
        return tuple(param_names)
    raise ValueError("Unrecognized Cudimot model %s", model_name)


