mcutils
=======

My personal library with a wide variety of tools and utilities

Usage
-----
Command line scripts are available through a single command: `mc_script`.

Installation
------------
```sh
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/mcutils.git
```

Documentation
-------------
Documentation can be found
[here](https://users.fmrib.ox.ac.uk/~ndcn0236/doc/mcutils/).

Authors
-------

`mcutils` was written by `Michiel Cottaar <MichielCottaar@protonmail.com>`_.
