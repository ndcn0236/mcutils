from fsl.utils import filetree
from mcutils.pipe import generates, Pipeline
import numpy as np
import nibabel as nib
from subprocess import call, run
import os.path as op
import sys
import logging
from fsl.utils.tempdir import tempdir
from contextlib import contextmanager
from pytest import raises
import os

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

nruns = 0


def _test_generate():
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    tree = filetree.read_tree_file('bet', directory='tmp_bet',
                                   name='test_image')
    filename = tree.get('input', make_dir=True)
    nib.Nifti1Image(np.random.rand(50, 50, 50), np.eye(4)).to_filename(filename)

    filetree.SUBMIT = True
    call(['rm', tree.get('output')])
    job_id = run_bet(tree)
    print(job_id)
    assert isinstance(job_id, tuple)
    assert op.isfile(tree.get('output'))
    assert len(job_id) == 1
    job_id2 = run_bet(tree)
    assert job_id == job_id2 or len(job_id2) == 0
    assert job_id is not job_id2

    tree.variables['test'] = 1
    call(['rm', tree.get('output')])
    job_id3 = run_bet(tree)
    assert job_id != job_id3
    assert op.isfile(tree.get('output'))
    assert nruns == 0, "submitted all jobs, so nothing ran locally"
    run_bet(tree, run=True)
    assert nruns == 1, "Forced local run without checking"

    tree.variables['test'] = 2
    call(['rm', tree.get('output')])
    filetree.SUBMIT = False
    job_id = run_bet(tree)
    print(job_id)
    assert len(job_id) == 0
    assert nruns == 2, "still running locally"
    job_id2 = run_bet(tree)
    assert len(job_id2) == 0
    assert nruns == 2, "no update needed"

    call(['rm', tree.get('output')])
    tree.variables['test'] = 3
    job_id3 = run_bet(tree)
    assert len(job_id3) == 0
    assert nruns == 3, "tree changed"
    call(['rm', '-r', 'tmp_bet'])
    call('rm run_bet*', shell=True)


@generates(['output'])
def run_bet(tree):
    global nruns
    call(['cp', tree.get('input'), tree.get('output')])
    nruns += 1


def test_io():
    tree = filetree.FileTree.read('bet')
    tree.variables['test_var'] = 7.8
    tree.save_pickle('tmp_bet.pck')
    tree2 = filetree.FileTree.load_pickle('tmp_bet.pck')
    assert tree.variables == tree2.variables
    assert tree.templates == tree2.templates
    call(['rm', 'tmp_bet.pck'])


def copy1(input, intermediary):
    assert not op.isfile(intermediary)
    run(['cp', input, intermediary], check=True)


def copy2(intermediary, output):
    assert not op.isfile(output)
    run(['cp', intermediary, output], check=True)


def copy3(intermediary, limited_output):
    assert not op.isfile(limited_output)
    run(['cp', intermediary, limited_output], check=True)


def copy4(in_2var, out_2var):
    assert not op.isfile(out_2var)
    run(['cp', in_2var, out_2var], check=True)


def summary(out_2var, sum_2var):
    print('producing', sum_2var)
    print(out_2var)
    assert not op.isfile(sum_2var)
    assert out_2var.size == 2
    with open(sum_2var, 'w') as f:
        for fn in out_2var.flatten():
            f.write(fn + '\n')
    run(['echo', sum_2var])


@contextmanager
def pipe_tester():
    with tempdir():
        tree = filetree.FileTree(
            templates={
                'input': 'sub-{subject}_in.txt',
                'intermediary': 'sub-{subject}_intermediary.txt',
                'output': 'sub-{subject}_out.txt',
                'limited_output': 'sub-{subject}_outl.txt',
                'in_2var': 'mvar_sub-{subject}_run-{run}_[opt-{opt}]_in.txt',
                'out_2var': 'mvar_sub-{subject}_run-{run}_[opt-{opt}]_out.txt',
                'sum_2var': 'mvar_sub-{subject}_summary.txt',
        },
            variables={},
        )
        with open(tree.update(subject='A').get('input'), 'w') as f:
            f.write('test A\n')
        with open(tree.update(subject='B').get('input'), 'w') as f:
            f.write('test B\n')

        for subject, run_id, opt in [
            ('A', '01', None),
            ('A', '02', None),
            ('B', '01', None),
            ('B', '02', None),
        ]:
            with open(tree.update(subject=subject, run=run_id, opt=opt).get('in_2var'), 'w') as f:
                f.write(f'test {subject} {run_id} {opt}\n')

        pipe = Pipeline(tree, {
            'subject': ('A', 'B'),
            'run': ('01', '02'),
        })

        pipe(in_files=['input'], out_files=['intermediary'])(copy1)

        pipe(in_files=['intermediary'], out_files=['output'])(copy2)

        pipe(in_files=['intermediary'], out_files=['limited_output'], possible_values={'subject': ['A']})(copy3)

        pipe(in_files=['in_2var'], out_files=['out_2var'])(copy4)

        pipe(in_files=['out_2var'], out_files=['sum_2var'], iter_vars=('run', 'opt'))(summary)

        yield pipe


def test_pipe():
    pipe: Pipeline
    _environ = dict(os.environ)
    try:
        if 'SGE_ROOT' in os.environ:
            del os.environ['SGE_ROOT']

        for method in ('single', ):
            print('testing', method)
            with pipe_tester() as pipe:
                pipe.run(['output'], catch=False, method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('output', glob_vars='all')) == 2

            with pipe_tester() as pipe:
                pipe.run(['intermediary', 'output'], catch=False, method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('output', glob_vars='all')) == 2

            with pipe_tester() as pipe:
                pipe.run(['output'], values={'subject': ['A']}, catch=False, method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('output', glob_vars='all')) == 1

            with pipe_tester() as pipe:
                pipe.run(['limited_output'], method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('intermediary', glob_vars='all')) == 1
                assert len(pipe.tree.get_all('limited_output', glob_vars='all')) == 1

            with pipe_tester() as pipe:
                pipe.run(['limited_output'], values={'subject': ['B']}, method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('intermediary', glob_vars='all')) == 0
                assert len(pipe.tree.get_all('limited_output', glob_vars='all')) == 0

            with pipe_tester() as pipe:
                pipe.run(['intermediary', 'limited_output'], method=method)
                assert len(pipe.tree.get_all('input', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('intermediary', glob_vars='all')) == 2
                assert len(pipe.tree.get_all('limited_output', glob_vars='all')) == 1

            with pipe_tester() as pipe:
                pipe.run(['out_2var'], catch=False, method=method)
                assert len(pipe.tree.get_all('in_2var', glob_vars='all')) == 4
                assert len(pipe.tree.get_all('out_2var', glob_vars='all')) == 4
                assert len(pipe.tree.get_all('sum_2var', glob_vars='all')) == 0

            with pipe_tester() as pipe:
                pipe.run(['sum_2var'], catch=False, method=method)
                assert len(pipe.tree.get_all('in_2var', glob_vars='all')) == 4
                assert len(pipe.tree.get_all('out_2var', glob_vars='all')) == 4
                assert len(pipe.tree.get_all('sum_2var', glob_vars='all')) == 2
    finally:
        os.environ.clear()
        os.environ.update(_environ)
