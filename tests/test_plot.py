from mcutils import plot
import numpy as np


def test_grid_shape():
    for ntotal, grid in [
        (1, (1, 1)),
        (2, (1, 2)),
        (3, (1, 3)),
        (4, (2, 2)),
        (5, (2, 3)),
        (6, (2, 3)),
        (7, (4, 2)),
        (8, (4, 2)),
        (9, (3, 3)),
        (10, (5, 2)),
        (11, (4, 3)),
        (21, (7, 3)),
    ]:
        assert plot._grid_shape(ntotal) == grid
