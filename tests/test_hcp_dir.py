#!/usr/bin/env python

from mcutils import hcp_dir
import os.path as op
from fsl.utils import filetree
import pytest


@pytest.mark.skipif(not (op.isdir('/Users/ndcn0236/Work/fmrib') or
                         op.isdir('/vols/Scratch/HCP/Diffusion')),
                    reason='Requires local HCP data')
def test_default_HCP_dirs():
    struct_dir = filetree.FileTree.read('HCP_directory', hcp_dir.HCP_dir('Structural'),
                                         subject='100307')
    diff_dir = filetree.FileTree.read('HCP_directory', hcp_dir.HCP_dir('Diffusion'),
                                       subject='100307')

    assert op.isfile(diff_dir.get('T1w_diffusion'))

    assert op.isfile(diff_dir.get('Diffusion/bvals'))
    assert op.isfile(diff_dir.get('bedpost/dyads1'))
    assert op.isfile(diff_dir.get('bedpost/nodif_brain_mask'))

    assert op.isfile(struct_dir.get('T1w_acpc_dc'))
    assert op.isfile(struct_dir.get('T1w'))
    assert op.isfile(struct_dir.update(hemi='L').get('mni_native/white'))
    assert op.isfile(struct_dir.update(hemi='R').get('mni_native/pial'))

    assert struct_dir.get('T1w') == struct_dir.get('mni_native/../T1w')
    assert struct_dir.get('T1w') == struct_dir.sub_trees['mni_native'].get('../T1w')
    assert (struct_dir.update(hemi='L').get('mni_native/white') ==
            struct_dir.sub_trees['mni_native'].update(hemi='L').get('white'))

    assert len(struct_dir.get_all('T1w_32k/inflated', glob_vars=['hemi'])) == 2
    assert len(struct_dir.update(hemi='L').get_all('T1w_32k/inflated')) == 1

    mid = struct_dir.get_all('T1w_native/mid', glob_vars=('hemi', ))
    for filename in mid:
        assert '100307' in filename

    assert set(struct_dir.all_variables.keys()) == set(('subject', ))
    assert set(struct_dir.sub_trees['T1w_32k'].all_variables.keys()) == set(('subject', 'space'))
    assert set(struct_dir.sub_trees['bedpost'].all_variables.keys()) == set(('subject', ))
