from mcutils.scripts.MDE import fit_dispersion, micro_anisotropy
import numpy as np
from numpy.testing import assert_allclose
from mcutils.utils.sidecar import AcquisitionParams
import time


def test_from_dti():
    V1 = np.random.randn(2, 3)
    V1 /= np.sqrt(np.sum(V1 ** 2, -1))[..., None]
    V2 = np.random.randn(2, 3)
    V2 -= (V1 * V2).sum(-1)[..., None] * V1
    V2 /= np.sqrt(np.sum(V2 ** 2, -1))[..., None]
    V3 = np.cross(V1, V2)

    angles = fit_dispersion.angles_from_dti(V1, V2, V3)
    rotmat = fit_dispersion.spherical.euler2mat(angles[..., 0], angles[..., 1], angles[..., 2])
    assert_allclose(rotmat.dot([0, 0, 1]), V1)
    assert_allclose(rotmat.dot([1, 0, 0]), -V3)
    assert_allclose(rotmat.dot([0, 1, 0]), V2)


def test_calc_dispersion():
    """
    Tests conversions between kappa dispersion parameter and angular dispersion
    """
    np.random.seed(12345)
    angle = np.random.rand(100) * 60
    assert_allclose(angle, fit_dispersion.calc_dispersion(fit_dispersion.calc_k(angle)), atol=4)
    k = np.random.rand(100) * 3
    assert_allclose(k, fit_dispersion.calc_k(fit_dispersion.calc_dispersion(k)), rtol=1e-4, atol=1e-5)


def test_stick_dispersion():
    base_bingham = fit_dispersion.bingham_matrix(0, 0, 0, 0, 0)
    assert base_bingham.dot([0, 0, 1]).dot([0, 0, 1]) == 0
    assert base_bingham.dot([1, 0, 0]).dot([1, 0, 0]) == -1
    assert base_bingham.dot([0, 1, 0]).dot([0, 1, 0]) == -1

    LTE = np.zeros((3, 3))
    LTE[2, 2] = 1
    b0 = np.zeros((3, 3))
    STE = np.eye(3)
    assert_allclose(fit_dispersion.stick_dispersion(0, STE, bingham=base_bingham), 1)
    assert_allclose(fit_dispersion.stick_dispersion(0, LTE, bingham=base_bingham), 1)
    assert_allclose(fit_dispersion.stick_dispersion(0, b0, bingham=base_bingham), 1)

    assert_allclose(fit_dispersion.stick_dispersion(1, STE, bingham=base_bingham), np.exp(-1))
    assert_allclose(fit_dispersion.stick_dispersion(1, b0, bingham=base_bingham), 1)

    for vars in np.random.randn(10, 5):
        bb = fit_dispersion.bingham_matrix(*vars)
        assert_allclose(fit_dispersion.stick_dispersion(1, STE, bingham=bb), np.exp(-1))
        assert_allclose(fit_dispersion.stick_dispersion(1, b0, bingham=bb), 1)


def test_zepp_signal():
    STE = np.eye(3)
    for _ in range(3):
        angles = np.random.randn(3) * 100
        params = [np.random.rand(), np.random.randn(), np.random.rand(), np.random.rand()]
        params2 = list(params)
        params2[2] = np.random.rand()
        assert_allclose(
                fit_dispersion.zepp_to_signal(angles, params, STE),
                fit_dispersion.zepp_to_signal(angles, params2, STE)
        )
        assert params != params2


def test_derivative():
    np.random.seed(1)
    bvec = np.random.randn(100, 3)
    bvec /= np.sqrt((bvec ** 2).sum(-1))[:, None]
    bmat = [
        np.concatenate([
            bvec[:, None, :] * bvec[:, :, None],
            [np.eye(3) / 3] * 10
        ], 0),
        np.concatenate([
            (np.eye(3) - bvec[:, None, :] * bvec[:, :, None]) / 2,
            [np.eye(3) / 3] * 10
        ], 0),
    ]

    bingham = fit_dispersion.bingham_matrix(2., 3., 0.4, 0.8, 1.2)

    # test stick_dispersion
    val, dS_dstick, dS_dbingham = fit_dispersion.stick_dispersion(3., bmat[0], bingham, derivative=True)
    assert_allclose(
            (fit_dispersion.stick_dispersion(3. + 1e-5, bmat[0], bingham) - val),
            dS_dstick * 1e-5, rtol=1e-3
    )
    for i in range(3):
        for j in range(3):
            if i >= j:
                bingham[i, j] += 1e-5
                bingham[j, i] += 1e-5
                assert_allclose(dS_dbingham[:, j, i], dS_dbingham[:, i, j])
                assert_allclose(
                        (fit_dispersion.stick_dispersion(3., bmat[0], bingham) - val),
                        2 * dS_dbingham[:, i, j] * 1e-5, rtol=3e-2, atol=1e-12
                )
                bingham[i, j] -= 1e-5
                bingham[j, i] -= 1e-5

    # test bingham derivative
    params = np.random.rand(5)
    val, dbh_dparams = fit_dispersion.bingham_matrix(*params, derivative=True)
    assert_allclose(
            val, fit_dispersion.bingham_matrix(*params)
    )
    for idx in range(5):
        params[idx] += 1e-5
        val2 = fit_dispersion.bingham_matrix(*params)
        assert_allclose(
                val2 - val,
                dbh_dparams[idx] * 1e-5, atol=1e-10, rtol=1e-4
        )
        params[idx] -= 1e-5

    # test zepp_to_signal
    angles = np.random.randn(3)
    params = np.random.randn(4)

    val, dS_dangles, dS_dparams = fit_dispersion.zepp_to_signal(angles, params, bmat[0], derivative=True)
    assert_allclose(
            val, fit_dispersion.zepp_to_signal(angles, params, bmat[0], derivative=False)
    )
    for idx in range(3):
        angles[idx] += 1e-5
        val2 = fit_dispersion.zepp_to_signal(angles, params, bmat[0], derivative=False)
        assert_allclose(
                (val2 - val)[:3],
                (1e-5 * dS_dangles[idx])[:3], rtol=1e-2, atol=1e-12
        )
        angles[idx] -= 1e-5
    for idx in range(len(params)):
        params[idx] += 1e-5
        val2 = fit_dispersion.zepp_to_signal(angles, params, bmat[0], derivative=False)
        assert_allclose(
                val2 - val,
                1e-5 * dS_dparams[idx], rtol=1e-2, atol=1e-10
        )
        params[idx] -= 1e-5

    fparams = np.concatenate((angles, params, params), 0)
    for idx in range(len(bmat)):
        assert_allclose(
                fit_dispersion.params_to_signal(fparams, bmat)[idx],
                fit_dispersion.zepp_to_signal(angles, params, bmat[idx])
        )

        assert_allclose(
                fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[0][idx],
                fit_dispersion.zepp_to_signal(angles, params, bmat[idx], derivative=True)[0]
        )

        assert_allclose(
                fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[1][idx][:3],
                fit_dispersion.zepp_to_signal(angles, params, bmat[idx], derivative=True)[1]
        )

        if idx == 0:
            assert_allclose(
                    fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[1][idx][3:7],
                    fit_dispersion.zepp_to_signal(angles, params, bmat[idx], derivative=True)[2]
            )
            assert_allclose(
                    fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[1][idx][7:],
                    0
            )
        else:
            assert_allclose(
                    fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[1][idx][3:7],
                    0
            )
            assert_allclose(
                    fit_dispersion.params_to_signal(fparams, bmat, derivative=True)[1][idx][7:],
                    fit_dispersion.zepp_to_signal(angles, params, bmat[idx], derivative=True)[2]
            )

    params = np.random.rand(11)
    # test params_to_signal
    val, der_params = fit_dispersion.params_to_signal(params, bmat, derivative=True)
    for v1, v2 in zip(val, fit_dispersion.params_to_signal(params, bmat, derivative=False)):
        assert_allclose(v1, v2)
    for idx in range(len(params)):
        params[idx] += 1e-5
        val2 = fit_dispersion.params_to_signal(params, bmat)
        for idx_sub, v1, v2, dp in zip(range(len(val)), val, val2, der_params):
            assert_allclose(
                    (v2 - v1)[0],
                    1e-5 * dp[idx, 0], rtol=1e-2, atol=1e-9
            )
        params[idx] -= 1e-5

    # test params_to_error
    observed = [np.zeros(bm.shape[0]) for bm in bmat]
    val, der_params = fit_dispersion.params_to_error(params, observed, bmat, derivative=True)
    assert_allclose(val, fit_dispersion.params_to_error(params, observed, bmat))
    for idx in range(len(params)):
        params[idx] += 1e-5
        val2 = fit_dispersion.params_to_error(params, observed, bmat)
        assert_allclose(
                (val2 - val) / 1e-5,
                der_params[idx], rtol=1e-2, atol=1e-5
        )
        params[idx] -= 1e-5

    start = time.time()
    [fit_dispersion.params_to_error(params, observed, bmat, derivative=False) for _ in range(10)]
    mid = time.time()
    print('without derivative:', (mid - start) / 10 * 1e3, 'ms')
    [fit_dispersion.params_to_error(params, observed, bmat, derivative=True) for _ in range(10)]
    print('with derivative:', (time.time() - mid) / 10 * 1e3, 'ms')


def test_non_central_derivatives():
    """
    Tests the derivatives for the non-central chi log(p)
    """
    np.random.seed(12)
    for include_S0 in (False, True):
        for ncoils in (1, 64):
            for _ in range(10):
                params = np.random.rand(12 + 2 * include_S0)

                np.random.seed(1)
                bvec = np.random.randn(100, 3)
                bvec /= np.sqrt((bvec ** 2).sum(-1))[:, None]
                bmat = [
                    np.concatenate([
                        bvec[:, None, :] * bvec[:, :, None],
                        [np.eye(3) / 3] * 10
                    ], 0),
                    np.concatenate([
                        (np.eye(3) - bvec[:, None, :] * bvec[:, :, None]) / 2,
                        [np.eye(3) / 3] * 10
                    ], 0),
                ]
                obs = fit_dispersion.params_to_signal(params[:-1], bmat, include_S0=include_S0)

                func, der_func = fit_dispersion.params_to_non_central_logp(params, obs, bmat,
                                                                           derivative=True, include_S0=include_S0,
                                                                           ncoils=ncoils)

                for idx in range(params.size):
                    params[idx] += 1e-4
                    f_up = fit_dispersion.params_to_non_central_logp(params, obs, bmat, include_S0=include_S0, ncoils=ncoils)
                    params[idx] -= 2e-4
                    f_down = fit_dispersion.params_to_non_central_logp(params, obs, bmat, include_S0=include_S0, ncoils=ncoils)
                    params[idx] += 1e-4
                    print(idx, der_func[idx], (f_up - f_down) / 2e-4)
                    assert_allclose((f_up + f_down) / 2, func, rtol=1e-2)
                    assert_allclose((f_up - f_down) / 2e-4, der_func[idx], rtol=1e-2)


def test_fit_dti():
    """
    Fit data generated from DTI
    """
    np.random.seed(1)
    bvec = np.random.randn(500, 3)
    bvec /= np.sqrt((bvec ** 2).sum(-1))[:, None]

    bmat = np.concatenate([
        bvec[:, None, :] * bvec[:, :, None],
        [np.eye(3) / 3] * 50
    ], 0) * 1e3

    xps = AcquisitionParams(btensor=bmat)

    # zeppelin diffusion tensor
    diff_tensor = np.diag([1, 1, 3]) * 1e-3
    signal = np.exp(-np.sum(bmat * diff_tensor, (-2, -1)))[None, :]

    _, res = fit_dispersion.run(signal, xps)

    assert_allclose(res.theta, 0, atol=1e-1)
    assert res.disp1[0] < 10
    assert res.disp2[0] < 10
    assert_allclose(res.anisotropy, 2e-3, atol=1e-1)
    assert_allclose(res.MDprime, 5e-3/3., atol=1e-1)

    bmat_S0 = np.concatenate([bmat, np.zeros((10, 3, 3))], 0)
    xps_S0 = AcquisitionParams(
            bt=bmat_S0[:, [0, 1, 2, 0, 0, 1], [0, 1, 2, 1, 2, 2]]
    )
    signal_S0 = np.concatenate([signal, np.full((1, 10), 2)], 1)
    _, res = fit_dispersion.run(signal_S0, xps_S0, include_S0=True)
    assert_allclose(res.theta, 0, atol=1e-1)
    assert res.disp1[0] < 10
    assert res.disp2[0] < 10
    assert_allclose(res.anisotropy, 2e-3, atol=1e-1)
    assert_allclose(res.MD, -np.log(1 + np.exp(-5/3)) / 1e3, atol=1e-1)
    assert_allclose(res.S0, 2, atol=1e-1)

    # crossing diffusion tensor
    diff_tensor1 = np.diag([3, 1, 1]) * 1e-3
    diff_tensor2 = np.diag([1, 3, 1]) * 1e-3
    signal = (
        np.exp(-np.sum(bmat * diff_tensor1, (-2, -1))) +
        np.exp(-np.sum(bmat * diff_tensor2, (-2, -1)))
    )[None, :] / 2

    _, res = fit_dispersion.run(signal, xps, dti_vectors=(
        [[1, 0, 0]], [[0, 1, 0]], [[0, 0, 1]]
    ))

    assert_allclose(res.theta, np.pi/2, atol=1e-1)
    assert_allclose(res.psi, 0, atol=1e-1)
    #assert res.disp1[0] < 10
    assert res.disp2[0] > 50
    assert_allclose(res.anisotropy, 2e-3, atol=1e-1)
    assert_allclose(res.MDprime, 5e-3/3, atol=2e-1)


def test_fit_uniform():
    """
    Tests fit_dispersion for uniform LTE data
    """
    np.random.seed(1)
    bvec = np.random.randn(300, 3)
    bvec /= np.sqrt((bvec ** 2).sum(-1))[:, None]
    bmat = np.concatenate([
        bvec[:, None, :] * bvec[:, :, None],
        [np.eye(3) / 3] * 200
    ], 0) * 1e3
    xps = AcquisitionParams(btensor=bmat)

    for ratio in (1, 1.5, 2):
        # no noise
        signal = np.append(np.ones(300, ) * ratio, np.ones(200, ))[None, :]
        _, res = fit_dispersion.run(signal, xps)
        print(res)

        anisotropy, Sbase = micro_anisotropy.run_single_shell(signal.T, xps['b_delta'], np.median(xps['b']))
        if ratio != 1:
            assert (res.log_k1 == -5).all()
            assert (res.log_k2 == -5).all()
        assert_allclose(res.anisotropy, anisotropy, atol=1e-8, rtol=5e-2)
        assert_allclose(res.MDprime, 0, atol=1e-8)
        assert_allclose(Sbase, 1, atol=1e-8)

        # Gaussian noise
        signal = np.append(np.ones(300, ) * ratio, np.ones(200, )) + np.random.randn(10, 500)
        _, res = fit_dispersion.run(signal, xps)

        anisotropy_noise, Sbase_noise = micro_anisotropy.run_single_shell(signal.T, xps['b_delta'], np.median(xps['b']))
        if ratio != 1:
            assert (res.disp1 > 40).all()
            assert (res.disp2 > 40).all()
        assert np.median(abs(res.anisotropy - anisotropy)) < 1e-3
        assert np.median(abs(anisotropy_noise - anisotropy)) < 1e-3
        assert_allclose(np.median(np.exp(-1000 * res.MDprime)), 1, rtol=0.1)
        assert_allclose(np.median(Sbase_noise), 1, atol=0.1)

        # Rician noise
        noise = np.random.randn(12, 10, 500) / 3
        signal_real = np.append(np.ones(300, ) * ratio, np.ones(200, ))
        signal = np.sqrt((noise[0] + signal_real) ** 2 + np.sum(noise[1:] ** 2, 0))
        _, res = fit_dispersion.run(signal, xps, ncoils=6, noise_var=1./3. ** 2)

        assert np.median(abs(res.anisotropy - anisotropy)) < 0.05
        arr = np.exp(-res.MDprime * 1000)
        assert_allclose(np.median(arr), 1, atol=np.std(arr))
        if ratio != 1:
            assert (res.disp1 > 40).all()
            assert (res.disp2 > 40).all()
