from mcutils.utils.scripts import ScriptDirectory
import importlib
from argparse import ArgumentParser
from operator import xor
import pytest


def test_import():
    script_dir = ScriptDirectory()
    assert len(list(script_dir)) > 15

    for module in script_dir:
        if module in (
            'gcoord.gui',
        ):
             continue
        print(module)
        filename, _ = script_dir.get(module.split('.'))

        script = importlib.import_module(filename)

        assert xor(
            hasattr(script, 'main'),
            hasattr(script, 'run_from_args') and hasattr(script, 'add_to_parser')
        ), "Found no or multiple ways to call the fuunction"
        if not hasattr(script, 'main'):
            parser = ArgumentParser()
            assert getattr(script, 'add_to_parser')(parser) is None
            assert isinstance(parser, ArgumentParser)

        with pytest.raises(SystemExit):
            script_dir([module, '-h'])
