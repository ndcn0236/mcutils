"""Fake docstring to read"""
from mcutils.utils.scripts import ScriptDirectory, get_docstring
from mcutils.scripts import __main__
import os.path as op


def test_get_docstring():
    assert get_docstring(__file__) == 'Fake docstring to read'


def test_script_directory():
    assert str(ScriptDirectory().directory) == op.split(__main__.__file__)[0]
    assert ScriptDirectory().in_module == 'mcutils.scripts'
    assert ScriptDirectory().name == 'scripts'

    assert 'MDE' in [sd.name for sd in ScriptDirectory()._script_groups()]
    assert (ScriptDirectory().directory / 'iter_link.py') in list(ScriptDirectory()._scripts())
    assert ScriptDirectory().get(['MDE'])[0].directory == ScriptDirectory().directory / 'MDE'
    assert ScriptDirectory().get(['MDE', 'btensor'])[0] == 'mcutils.scripts.MDE.btensor'
