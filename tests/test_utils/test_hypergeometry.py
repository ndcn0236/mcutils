from mcutils.utils import hypergeometry, spherical
import numpy as np
from scipy import integrate
from numpy.testing import assert_allclose


def test_numeric_integral():
    """
    Compare exact with numeric integral
    """
    np.random.seed(1)
    bingham_mat = np.eye(3)
    bingham_mat[2, 2] = 0

    def evaluate(bingham_mat, phi, theta):
        bvec = spherical.spherical2cart(1, phi, theta)
        value_mult = bingham_mat.dot(bvec).dot(bvec)
        return np.exp(-value_mult)

    assert_allclose(evaluate(bingham_mat, 0, 0), 1)
    assert_allclose(evaluate(bingham_mat, 0, np.pi), 1)
    assert_allclose(evaluate(bingham_mat, 0, np.pi / 2), np.exp(-1))
    assert_allclose(evaluate(bingham_mat, np.pi / 2, np.pi / 2), np.exp(-1))

    func = lambda phi, theta: evaluate(np.zeros((3, 3)), phi, theta) * np.sin(theta)
    assert_allclose(integrate.dblquad(func, 0, np.pi / 2, lambda theta: 0, lambda theta: 2 * np.pi)[0],
                    2 * np.pi)

    func = lambda phi, theta: evaluate(bingham_mat, phi, theta) * np.sin(theta)
    assert_allclose(
            integrate.dblquad(func, 0, np.pi / 2, lambda theta: 0, lambda theta: 2 * np.pi)[0],
            integrate.dblquad(func, np.pi / 2, np.pi, lambda theta: 0, lambda theta: 2 * np.pi)[0],
    )
    assert_allclose(
            integrate.dblquad(func, 0, np.pi / 2, lambda theta: 0, lambda theta: 2 * np.pi)[0],
            integrate.dblquad(func, 0, np.pi, lambda theta: 0, lambda theta: np.pi)[0],
    )
    assert_allclose(
            hypergeometry.bingham_normalization(-bingham_mat),
            hypergeometry.bingham_normalization([-bingham_mat])
    )
    assert_allclose(
            hypergeometry.bingham_normalization(np.zeros((3, 3))), 1
    )
    for _ in range(3):
        bingham_mat = np.diag(np.random.randn(3))
        # typical precision: <0.1% up to 5%
        assert_allclose(
                integrate.dblquad(func, 0, np.pi, lambda theta: 0, lambda theta: 2 * np.pi)[0],
                hypergeometry.bingham_normalization(-bingham_mat) * 4 * np.pi, rtol=0.1
        )


def test_derivative():
    """
    Tests the calculation of the derivative
    """
    A = np.eye(3)
    A[-1, -1] = 0
    val1, derivative = hypergeometry.der_bingham_normalization(A)

    assert_allclose(derivative[0, 0], derivative[1, 1])
    temp = derivative.copy()
    np.fill_diagonal(temp, 0.)
    assert_allclose(temp, 0)

    for i in range(3):
        A[i, i] += 1e-5
        val2 = hypergeometry.bingham_normalization(A)
        assert_allclose((val2 - val1) / 1e-5, derivative[i, i], rtol=1e-2)
        A[i, i] -= 1e-5
