from mcutils.utils import sidecar
import numpy as np
from numpy.testing import assert_allclose
import os.path as op
from subprocess import run
from pytest import raises


def test_convert():
    bt = [
        (1, 0, 0, 0, 0, 0),
        (0, 0.5, 0.5, 0, 0, 0)
    ]
    res = sidecar.AcquisitionParams(bt=bt)
    assert res.n == 2
    assert sidecar.concat(res, res).n == 4
    assert_allclose(res['bt'], bt)
    assert_allclose(res['b'], 1)
    assert_allclose(res['b_passxx'], (0, 0.5))
    assert_allclose(res['b_passyy'], (0, 0.5))
    assert_allclose(res['b_passzz'], (1, 0))
    assert_allclose(res['b_delta'], (1, -0.5))
    assert_allclose(res['b_eta'], (0, 0))
    btensor = np.zeros((2, 3, 3))
    btensor[0, 0, 0] = 1
    btensor[1, 1, 1] = 0.5
    btensor[1, 2, 2] = 0.5
    assert_allclose(res['btensor'], btensor)

    res['b'] = 2
    print(res['b'])
    assert_allclose(res['b'], 2)
    with raises(ValueError):
        res.check()
    res['b'] = 1
    assert_allclose(res['b'], 1)
    res.check()
    assert_allclose(res['bt'], bt)

    res = sidecar.AcquisitionParams({
        'b': 3,
        'b_delta': -0.5,
        'b_eta': -0.1,
    })
    res.check()
    assert len(res) == 6
    assert_allclose(res['b_passzz'], 0)
    assert_allclose(res['b_passyy'], 1.55)
    assert_allclose(res['b_passxx'], 1.45)

    res.update(theta=0, phi=0)
    res.check()
    assert len(res) == 9
    assert_allclose(res['bvec'], [[0, 0, 1]])

    res.update(psi=0)
    res.check()
    assert len(res) == 13
    assert_allclose(res['bvec2'], [[1, 0, 0]])
    assert_allclose(res['btensor'], [[
        [1.45, 0, 0],
        [0, 1.55, 0],
        [0, 0, 0]
    ]])
    assert_allclose(res['bt'], [[1.45, 1.55, 0, 0, 0, 0]])


def test_pandas():
    xps = sidecar.AcquisitionParams({'te': (10, 20)})
    xps.check()
    assert(len(xps) == 1)

    xps.to_pandas().te == (10, 20)
    through_pandas = xps.from_pandas(xps.to_pandas())
    assert len(through_pandas) == len(xps)
    for name in xps.keys():
        assert_allclose(through_pandas[name], xps[name])

    xps['bt'] = [
        (1, 0, 0, 0, 0, 0),
        (0, 0.5, 0.5, 0, 0, 0)
    ]
    xps.check()

    through_pandas = xps.from_pandas(xps.to_pandas())
    through_pandas.auto_update()
    assert len(through_pandas) == len(xps)
    for name in xps.keys():
        assert_allclose(through_pandas[name], xps[name], atol=1e-8)


def test_io_matlab():
    directory = op.split(__file__)[0]
    xps = sidecar.AcquisitionParams.from_mat(op.join(directory, 'test_xps.mat'))
    assert_allclose(xps['b'], np.array((2, 2, 2, 3)) * 1e6)
    assert_allclose(xps['bt'][0], np.array((0, 0, 2, 0, 0, 0)) * 1e6)

    new_fn = op.join(directory, 'test_xps_temp.mat')
    xps.to_mat(new_fn)
    new_xps = sidecar.AcquisitionParams.from_mat(new_fn)
    assert new_xps == xps
    assert sidecar.AcquisitionParams.read(new_fn) == xps
    run(['rm', new_fn])


def test_io_json():
    bt = np.zeros((2, 6))
    bt[0] = 1.
    xps = sidecar.AcquisitionParams(te=(10, 20), bt=bt)
    xps.check()

    directory = op.split(__file__)[0]
    new_fn = op.join(directory, 'test_xps_temp.json')
    xps.write(new_fn)
    assert xps.read(new_fn) == xps
    assert xps.from_json(new_fn) == xps
    run(['rm', new_fn])


def test_read_bvals_bvecs():
    directory = op.split(__file__)[0]
    xps = sidecar.AcquisitionParams.read_bvals_bvecs(
            op.join(directory, 'bvals'),
            op.join(directory, 'bvecs'),
    )

    ref_raw = sidecar.AcquisitionParams.from_mat(op.join(directory, 'test_xps.mat'))
    ref = sidecar.AcquisitionParams(bt=ref_raw['bt'] / 1e6)

    assert 'btensor' in ref
    assert 'btensor' in xps
    assert xps == ref

    xps_pte = sidecar.AcquisitionParams.read_bvals_bvecs(
            op.join(directory, 'bvals'),
            op.join(directory, 'bvecs'),
            modality='PTE'
    )
    assert_allclose((np.asarray(xps_pte['btensor']) * xps['btensor']).sum((-1, -2)), 0, atol=1e-8)

    xps_ste = sidecar.AcquisitionParams.read_bvals_bvecs(
            op.join(directory, 'bvals'),
            modality='STE'
    )
    assert_allclose(xps_ste['btensor'], np.eye(3) * ref['b'][:, None, None] / 3, atol=1e-8)

    for xps_test in (xps, xps_pte, xps_ste):
        assert len(xps_test) == 13
        assert_allclose(
                np.trace(xps_test['btensor'], axis1=-1, axis2=-2),
                ref['b']
        )


def test_index_xps():
    """
    Tests the indexing of XPS objects
    """
    bvals = np.array([0, 0, 0, 100, 200, 150, 500, 600, 500, 0])
    te    = np.array([0, 0, 1,   0,   0,   1,   0,   0,   0, 1])
    xps = sidecar.AcquisitionParams(b=bvals, te=te)

    assert xps.n == 10
    assert (xps['b'] == bvals).all()
    assert (xps['te'] == te).all()
    assert (xps[0]['b'] == 0).all()
    assert (xps[:3]['b'] == 0).all()
    assert (xps[3]['b'] == 100).all()
    assert (xps[[6, -2]]['b'] == 500).all()

    index = xps.get_index(b=50)
    assert (index == (0, 0, 0, 1, 1, 1, 2, 3, 2, 0)).all()
    assert (index == xps.get_index(b=50, te=1)).all()
    assert (index == xps.get_index(b=50, te=1, sort_by='b')).all()
    te = xps.groupby(xps.get_index(b=50, sort_by='te'))['te']
    print(te)
    assert (np.sort(te) == te).all()
    te = xps.groupby(xps.get_index(b=50, te=1, sort_by='te'))['te']
    assert (np.sort(te) == te).all()
    te = xps.groupby(xps.get_index(b=50, te=0.5, sort_by='te'))['te']
    assert (np.sort(te) == te).all()
    assert xps.groupby(index).n == max(index) + 1

    index = xps.get_index(b=50, te=0)
    assert (index == (0, 0, 1, 2, 3, 4, 5, 6, 5, 1)).all()
    assert xps.groupby(index).n == max(index) + 1

    bt = [
        (1, 0, 0, 0, 0, 0),
        (0, 1, 0, 0, 0, 0)
    ]
    xps = sidecar.AcquisitionParams(bt=bt)
    xps.check()
    assert_allclose(xps['b_delta'], 1)
    assert_allclose(xps.groupby([0, 0])['b_delta'], 1)
    assert 'btensor' in xps
    assert 'btensor' in xps.groupby([0, 0])
    assert 'btensor' not in xps.groupby([0, 0], drop='theta')
