.. mcutils documentation master file, created by
   sphinx-quickstart on Fri Nov 23 14:59:48 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to documentation for mcutils!
=====================================

.. automodule:: mcutils
    :members:
    :undoc-members:
    :show-inheritance:

.. toctree::
   :maxdepth: 1

   scripts
   utilities

