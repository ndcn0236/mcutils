Utilities in mcutils package
============================

.. automodule:: mcutils
    :noindex:

    .. rubric:: Pipeline
    .. autosummary::
        :toctree: _utilities
        :template: utilities.rst

        pipe
        cudimot
        register.multi_image
        hcp_dir

    .. rubric:: Plotting
    .. autosummary::
        :toctree: _utilities
        :template: utilities.rst

        plot
        utils.colour

    .. rubric:: Maths
    .. autosummary::
        :toctree: _utilities
        :template: utilities.rst

        utils.noise
        utils.hypergeometry
        utils.kmedoids
        utils.spherical

    .. rubric:: Input/Output
    .. autosummary::
        :toctree: _utilities
        :template: utilities.rst

        utils.cifti
        utils.greyordinate
        utils.write_gifti
        utils.trajectory
        utils.sidecar
        utils.wb_scene

    .. rubric:: Not neuroimage
    .. autosummary::
        :toctree: _utilities
        :template: utilities.rst

        sync
        utils.log
        utils.bibtex
        utils.scripts
