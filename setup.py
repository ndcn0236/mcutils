import setuptools
import os.path as op
import shutil


basedir = op.dirname(__file__)


class doc(setuptools.Command):
    """Build the API documentation. """

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        import sphinx.cmd.build as sphinx_build

        docdir  = op.join(basedir, 'doc')
        destdir = op.join(docdir, 'html')

        if op.exists(destdir):
            shutil.rmtree(destdir)

        print('Building documentation [{}]'.format(destdir))

        sphinx_build.main([docdir, destdir])


setuptools.setup(
    name="mcutils",
    version="0.1.0",
    url="https://git.fmrib.ox.ac.uk/ndcn0236/mcutils.git",

    author="Michiel Cottaar",
    author_email="MichielCottaar@protonmail.com",

    description="My personal utilities library",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(exclude=('tests*', )),

    zip_safe=False,

    package_data={'mcutils': ['trees/*']},

    install_requires=[
        'six', 'fslpy', 'numpy', 'nibabel', 'seaborn', 'graphviz',
        'numba', 'matplotlib', 'pandas', 'dipy', 'mpmath',
        'scikit-image', 'scikit-learn', 'dask[array]', 'cifti',
    ],

    include_package_data=True,

    entry_points={'console_scripts': [
        'mc_script=mcutils.scripts.__main__:main',
    ]},

    cmdclass={'doc': doc},

    extras_require={
        'greyordinates': ['zarr', 'h5py'],
        'gcoord': ['pymc3'],
        'bibtex': ['metapub', 'selenium', 'eutils', 'traitlets', 'arxiv', 'ads'],
        'doc': ['sphinx', 'sphinx-rtd-theme'],
    },

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
